﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Assets.Scripts.Automaton.Expr;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Scripts.Iterator;

namespace Assets.Scripts.Automaton.Expr.Tests
{
	[TestClass()]
	public class ExprTests
	{
		private CallExecutor executor;

		[TestInitialize()]
		public void TestInitialize()
		{
			executor = (name, parameters) => (bool) GetType().GetMethod(name).Invoke(this, parameters);
		}

		[TestMethod()]
		public void ParseTest()
		{
			const string a = "t() & f() |t() &f()";
			Trace.WriteLine(a);
			//Global.print(Expr.Expr.Parse(new ArrayIterator<char>(a.ToCharArray()), (s, ab) => true).ToString());
			Expr expr = Expr.Parse(new ArrayIterator<char>(a.ToCharArray()), executor);
			Assert.IsTrue(expr.Execute());
		}

		[TestMethod()]
		public void ExecuteTest()
		{
			const string a = "isAB(A)";
			Trace.WriteLine(a);
			//Global.print(Expr.Expr.Parse(new ArrayIterator<char>(a.ToCharArray()), (s, ab) => true).ToString());
			Expr expr = Expr.Parse(new ArrayIterator<char>(a.ToCharArray()), executor);
			Assert.IsTrue(expr.Execute());
		}

		public bool t() => true;
		public bool f() => false;
		public bool isA(string a) => a == "A";
		public bool isAB(string a, string b) => a == "A" && b == "B";
	}
}