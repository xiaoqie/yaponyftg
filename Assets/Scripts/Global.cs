﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

/**
* This scene uses a strange coordinate system:
* ^+y
* |
* |----->
*       -x
* Up is +y, Right is -x!!!!!! (by mistake)
*/
namespace Assets.Scripts
{
	public class Global : MonoBehaviour
	{

		public Text Tl;
		public Text Tr;
		public Text Bl;
		public Text Br;
		public GameObject PrefabTs;
		public Camera MainCamera;
		public Image P1Health;
		public Image P2Health;
		public AudioClip Punch1;

		private new AudioSource audio;

		private GameObject _player1;
		private GameObject _player2;

		public static Global Instance;

		// Use this for initialization
		void Start()
		{
			Destroy(GameObject.Find("TwilightSparkle"));

			Instance = this;

			audio = GetComponent<AudioSource>();

			_player1 = _instantiatePlayer(PrefabTs, "1");
			_player2 = _instantiatePlayer(PrefabTs, "2");

			_player1.GetComponent<BaseController>().opponent = _player2.GetComponent<TSController>();
			_player2.GetComponent<BaseController>().opponent = _player1.GetComponent<TSController>();

			//SetLayer(_player1, LayerMask.NameToLayer("Player1"));
			//SetLayer(_player2, LayerMask.NameToLayer("Player2"));

			StartCoroutine(DisplayFps());

			ShowDebugMessageOnScreen("DEBUG MESSAGE!");
		}

		GameObject _instantiatePlayer(GameObject prefab, string id)
		{
			int sign = id == "1" ? 1 : -1;

			GameObject player = (GameObject)Instantiate(prefab, new Vector3(sign * 4f, 0.7f, 7f), Quaternion.identity);
			player.GetComponent<BaseController>().PlayerId = id;
			player.GetComponent<Transform>().eulerAngles = new Vector3(90, -sign * 90, 0);
			player.GetComponent<BaseController>().health = id == "1" ? P1Health.GetComponent<HealthBar>() : P2Health.GetComponent<HealthBar>();

			return player;
		}
	
		// Update is called once per frame
		void Update()
		{
			string[] keys = { "x1", "y1", "a", "b", "x", "y", "l", "r" };

			var DisplayText =
				new Action<Text, IEnumerable<KeyValuePair<string, object>>>((txt, dicts) =>
				{
					StringBuilder sb = new StringBuilder();
					foreach (var pair in dicts)
					{
						sb.AppendFormat("{0}: {1}", pair.Key, pair.Value);
						sb.AppendLine();
					}
					txt.text = sb.ToString();
				});
			DisplayText(Tl, from key in keys select new KeyValuePair<string, object>("p1" + key, Input.GetAxis("p1" + key)));
			Tl.text = Tl.text + "isGrounedP1:" + _player1.GetComponent<TSController>().IsGrounded();
			DisplayText(Tr, from key in keys select new KeyValuePair<string, object>("p2" + key, Input.GetAxis("p2" + key)));
			Tr.text = Tr.text + "isGrounedP2:" + _player2.GetComponent<TSController>().IsGrounded();

			/** Comment before I forget:
		 *-------------------------------> x-axis
		 *               |
		 *               | 7 units(that's where the very first 7 comes from)
		 *               |
		 *A__________M__________B viewport
		 * -         |         - 
		 *    -      |      -
		 *       -   |   -
		 *           -
		 *           O
		 * O is the camera, A and B are players.
		 * The angle OAM doesn't change.
		 * So AM_init     OM_init
		 *   --------- = ---------  where AM_init = 6, OM_init = 7
		 *    AM_now      OM_now
		 * OM is the camera_z we needed.
		 * 
		 * I calculate x,y separately.
		 * The camera is 3 units above players, so we must add a +3y to the upper player, a -3y to the lower player. +6/2 in total.
		 * (MAYBE I SHOULD USE CONSTANTS DEFINED INSTEAD OF NUMBERS)
		 */
			Vector3 newPosition = (_player1.transform.position + _player2.transform.position) / 2;
			newPosition.z = 7 + Mathf.Max(7,
				(Mathf.Abs(_player1.transform.position.x - _player2.transform.position.x) / 2) / 6 * 7, 
				(Mathf.Abs(_player1.transform.position.y - _player2.transform.position.y) / 2 + 3) / 6 * 7);
			newPosition.y = newPosition.y + 3;
			MainCamera.transform.position = newPosition;
		}

		private IEnumerator DisplayFps()
		{
			while (true)
			{
				int lastFrameCount = Time.frameCount;
				yield return new WaitForSeconds(1);
				Bl.text = "FPS:" + (Time.frameCount - lastFrameCount);
			}
		}

		public static void ShowDebugMessageOnScreen(object text)
		{
			Instance.Br.text = text.ToString();
		}

		public static void SetLayer(GameObject o, int layer)
		{
			o.layer = layer; 
			foreach (Transform child in o.GetComponentInChildren<Transform>(true))
			{
				child.gameObject.layer = layer;
			}
		}

		public static IEnumerator PauseScreenFor(float p)
		{
			Time.timeScale = 0f;
			float pauseEndTime = Time.realtimeSinceStartup + p;
			while (Time.realtimeSinceStartup < pauseEndTime)
			{
				yield return 0;
			}
			Time.timeScale = 1;
		}

		public static void DealDamageTo([CanBeNull] GameObject performer, [NotNull] GameObject sufferer, [NotNull] Damage detail)
		{
			if (!sufferer.CompareTag("PlayerBody") && !sufferer.CompareTag("PlayerHead") && !sufferer.CompareTag("Player"))
				throw new NotImplementedException("sufferer is not any player");

			switch (detail.type)
			{
				case Damage.Type.STUN:
					sufferer.GetComponentInParent<Animator>().Play(sufferer.CompareTag("PlayerHead") ? "headdamage" : "littledamage", 0);
					break;
				case Damage.Type.KNOCK_DOWN:
					sufferer.GetComponentInParent<Animator>().Play("knockdown", 0);
					sufferer.GetComponentInParent<Rigidbody>().AddForce(0, 3.5f, 0, ForceMode.Impulse);
					break;
				case Damage.Type.KNOCK_BACK:
					sufferer.GetComponentInParent<Animator>().Play("knock_back", 0);
					sufferer.GetComponentInParent<Rigidbody>().AddForce(0, 0.5f, 0, ForceMode.Impulse);
					break;
				case Damage.Type.LAUNCHER:
					sufferer.GetComponentInParent<Animator>().Play("launcher", 0);
					sufferer.GetComponentInParent<Rigidbody>().AddForce(0, 10f, 0, ForceMode.Impulse);
					break;
				case Damage.Type.NO:
					break;
			}
			if (performer == null)
			{
				if  (detail.forceHorizontalDirection != Vector3.zero)
				{
					sufferer.GetComponentInParent<Rigidbody>()
						.AddForce(detail.horizontalForce*detail.forceHorizontalDirection + detail.verticalForce*Vector3.up,
							ForceMode.Impulse);
				}
			}
			else if (detail.forceHorizontalDirection == Vector3.zero)
			{
				Vector3 horizontalForceDir = new Vector3((sufferer.transform.position.x - performer.transform.position.x).Sign(), 0, 0);

				sufferer.GetComponentInParent<Rigidbody>()
					.AddForce(detail.horizontalForce*horizontalForceDir + detail.verticalForce*Vector3.up,
						ForceMode.Impulse);

				performer.GetComponent<Rigidbody>()
					.AddForce(-detail.horizontalForce*horizontalForceDir + detail.verticalForce*Vector3.up,
						ForceMode.Impulse);
			}
			else
			{
				sufferer.GetComponentInParent<Rigidbody>()
					.AddForce(detail.horizontalForce*detail.forceHorizontalDirection + detail.verticalForce*Vector3.up,
						ForceMode.Impulse);

				performer.GetComponent<Rigidbody>()
					.AddForce(-detail.horizontalForce*detail.forceHorizontalDirection + detail.verticalForce*Vector3.up,
						ForceMode.Impulse);
			}
			sufferer.GetComponentInParent<BaseController>().health.Reduce(detail.damage);

			Instance.audio.PlayOneShot(Instance.Punch1);
			Instance.StartCoroutine(Global.PauseScreenFor(detail.screenFrozenTime));
		}
	}
}
