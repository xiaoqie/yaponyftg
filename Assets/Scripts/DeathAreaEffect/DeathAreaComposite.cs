﻿using UnityEngine;

public class DeathAreaComposite : MonoBehaviour
{
	[Range (0, 10)]
	public float Intensity = 1;

	private Material _compositeMat;

	void OnEnable()
	{
		_compositeMat = new Material(Shader.Find("Hidden/DeathAreaComposite"));
    }

	void OnRenderImage(RenderTexture src, RenderTexture dst)
	{
		_compositeMat.SetFloat("_Intensity", Intensity);
        Graphics.Blit(src, dst, _compositeMat, 0);
	}
}
