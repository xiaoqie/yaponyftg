﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using static U;
using static UnityEngine.Mathf;
using V = UnityEngine.Vector3;

public class DeathAreaPrePass : MonoBehaviour {
    private RenderTexture PrePass;
    public string name; // very important

    private Material _blurMat;
    void Start() {
        print(Screen.width);
        print(Screen.width >> 2);
        PrePass = new RenderTexture(1920 >> 2, 1080 >> 2, 24);

        var camera = GetComponent<Camera>();

        camera.targetTexture = PrePass;
        Shader.SetGlobalTexture($"_{name}AreaTex", PrePass);


        _blurMat = new Material(Shader.Find("Hidden/Blur"));
        _blurMat.SetVector("_BlurSize", new Vector2(PrePass.texelSize.x * 1.5f, PrePass.texelSize.y * 1.5f));
    }

    void OnRenderImage(RenderTexture src, RenderTexture dst) {
        Graphics.Blit(src, dst);

        Graphics.SetRenderTarget(PrePass);
        GL.Clear(false, true, Color.clear);

        Graphics.Blit(src, PrePass);
        
        for (int i = 0; i < (name == "Safe" ? 10 : 50); i++) {
            var temp = RenderTexture.GetTemporary(PrePass.width, PrePass.height);
            Graphics.Blit(PrePass, temp, _blurMat, 0);
            Graphics.Blit(temp, PrePass, _blurMat, 1);
            RenderTexture.ReleaseTemporary(temp);
        }
    }
}
