using System;
using System.Collections.Generic;
using System.Xml;
using Assets.Scripts.Iterator;
using Assets.Scripts.Automaton.Expr;

/**
 * Documentation:
 * The xml begins with "automaton" node.
 * A root node must be in the "automaton" node, with no condition.
 * Nodes:
 *	State node: A regular state, may have (in state called every frame) action, and an enter state action.
 *	End node: A state, may have (in state) action(which is called once), and an enter state action, return to root after execution.
 *	Action node: A state, may have (in state) action(which is called once), and an enter state action.
 * TODO: TIMER, COMPILE CONDITION
 */

namespace Assets.Scripts.Automaton
{
	public class State
	{
		private List<State> _successors = new List<State>();
		private State _root;
		private string _reference = "";
		private string _name = ""; // debug info
		private string _type;

		private Func<bool> _condition;
		public Action DoAction = () => { };
		public Action DoEnterState = () => { };

		public State(Func<bool> cond)
		{
			_condition = cond;
		}

		public void AddSuccessor(State state)
		{
			_successors.Add(state);
		}

		public State Next()
		{
			foreach (State state in _successors)
			{
				if (state._condition())
				{
					state.DoEnterState();
					state.DoAction();
					if (state._type == "action")
					{
						return this;
					}
					if (state._type == "end")
					{
						return state._root;
					}
					if (state._type == "state")
					{
						return state;
					}
					throw new ArgumentException("wrong state name");
				}
			}
			DoAction();
			return this;
		}

		public static State ReadFromConfigFile(string configFile, object funcs)
		{
			Dictionary<string, State> stateMapping = new Dictionary<string, State>();

			XmlDocument config = new XmlDocument();
			config.LoadXml(configFile);
			XmlElement rootXmlNode = (XmlElement) config.FirstChild;

			State root = ReadState((XmlElement) rootXmlNode.FirstChild, funcs, stateMapping);
			root._root = root;
			ProcessReference(root, stateMapping);
			return root;
		}

		private static void ProcessReference(State state, Dictionary<string, State> stateMapping)
		{
			for (int i = 0; i < state._successors.Count; i++)
			{
				State successor = state._successors[i];
				state._successors[i]._root = state._root;
				if (successor._reference == "")
				{
					ProcessReference(successor, stateMapping);
				}
				else
				{
					state._successors[i] = stateMapping[successor._reference];
				}
			}
		}

		private static State ReadState(XmlElement node, object funcs, Dictionary<string, State> stateMapping)
		{
			State state = null;
			string reference = node.GetAttribute("reference");
			if (reference != "")
			{
				state = new State(null);
				state._reference = reference; // declaration
			}
			else
			{
				string condition = node.GetAttribute("condition");
				Expr.Expr e = Expr.Expr.Parse(new ArrayIterator<char>(condition.ToCharArray()),
					(methodName, parameters) =>
					{
					   if (funcs.GetType().GetMethod(methodName) == null)
						   throw new NullReferenceException("method not found:" + methodName);
						return (bool) funcs.GetType().GetMethod(methodName).Invoke(funcs, parameters);
					});
				state = new State(() => e.Execute());

				string enterStateAction = node.GetAttribute("enterStateAction");
				if (enterStateAction != "")
				{
					Expr.Expr eact = Expr.Expr.Parse(new ArrayIterator<char>(enterStateAction.ToCharArray()),
						   (methodName, parameters) =>
						   {
							   if (funcs.GetType().GetMethod(methodName) == null)
								   throw new NullReferenceException("method not found:" + methodName);
							   funcs.GetType().GetMethod(methodName).Invoke(funcs, parameters);
							   return true;
						   });
					state.DoEnterState = () => eact.Execute();
					//state.DoEnterState = () => { funcs.GetType().GetMethod(enterStateAction).Invoke(funcs, null); };
				}

				string action = node.GetAttribute("action");
				if (action != "")
				{
					Expr.Expr eact = Expr.Expr.Parse(new ArrayIterator<char>(action.ToCharArray()),
						(methodName, parameters) =>
						{
							if (funcs.GetType().GetMethod(methodName) == null)
								throw new NullReferenceException("method not found:" + methodName);
							funcs.GetType().GetMethod(methodName).Invoke(funcs, parameters);
							return true;
						});
					//state.DoAction = () => { funcs.GetType().GetMethod(action).Invoke(funcs, null); };
					state.DoAction = () => eact.Execute();
				}

				string name = node.GetAttribute("name");
				state._name = name;

				string type = node.Name;
				state._type = type;
				//PlayerController.print(name + action + condition);

				if (name != "")
					stateMapping.Add(name, state);
			}
			if (node.HasChildNodes)
			{
				for (int i = 0; i < node.ChildNodes.Count; i++)
				{
					state.AddSuccessor(ReadState((XmlElement) node.ChildNodes[i], funcs, stateMapping));
				}
			}
			return state;
		}
	}
}