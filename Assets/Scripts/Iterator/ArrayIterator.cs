namespace Assets.Scripts.Iterator
{
	public class ArrayIterator<TU> : IIterator<TU>
	{
		private readonly TU[] _array;
		private int _position;
		private readonly int _length;

		public ArrayIterator(TU[] array)
		{
			_array = array;
			_length = array.Length;
		}

		public bool HasNext()
		{
			return _position < _length;
		}

		public bool MoveNext()
		{
			_position++;
			return HasNext();
		}

		public TU Current()
		{
			return _array[_position];
		}
	}
}