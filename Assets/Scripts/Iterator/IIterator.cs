﻿namespace Assets.Scripts.Iterator
{
	public interface IIterator<TU>
	{
		bool HasNext();
		bool MoveNext();
		TU Current();
	}
}