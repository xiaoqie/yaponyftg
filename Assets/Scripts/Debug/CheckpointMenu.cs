﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using static U;
using static UnityEngine.Mathf;
using V = UnityEngine.Vector3;

public class CheckpointMenu : MonoBehaviour {
    private void Start() {
        var text = transform.Find("InputField/Text").GetText();
        transform.Find("Button").GetButton().onClick.AddListener(() => {
            int result;
            if (int.TryParse(text.text, out result))
                SceneParameters.Checkpoint2Load = result;
            UnityEngine.SceneManagement.SceneManager.LoadScene(0);
        });
    }
}
