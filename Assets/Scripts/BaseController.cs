﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using Assets.Scripts.Automaton;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
	public class BaseController : MonoBehaviour
	{
		private Vector3 _turningVelocity;

		protected struct ControlStruct
		{
			public string AxisX, AxisY, A, B, X, Y, L, R;
		}

		protected ControlStruct Control;

		protected Animator animator;
		protected Rigidbody rb;
		protected State state;
		protected Dictionary<string, float> timers = new Dictionary<string, float>();
		protected Dictionary<string, GameObject> OffensiveObjects = new Dictionary<string, GameObject>();

		protected Damage nextDamage = new Damage();

		public string PlayerId = "1";

		public enum Orientation { Left = 1, Right = -1 }
		public Orientation orientation;
		public BaseController opponent;
		public HealthBar health;

		// Use this for initialization
		protected virtual void Start()
		{
			Control.AxisX = "p" + PlayerId + "x1";
			Control.AxisY = "p" + PlayerId + "y1";
			Control.A = "p" + PlayerId + "a";
			Control.B = "p" + PlayerId + "b";
			Control.X = "p" + PlayerId + "x";
			Control.Y = "p" + PlayerId + "y";
			Control.L = "p" + PlayerId + "l";
			Control.R = "p" + PlayerId + "r";

			animator = GetComponent<Animator>();
			rb = GetComponent<Rigidbody>();

			orientation = Orientation.Left;

			// i know, performance, but called only twice;

			OffensiveObjects["RightFoot"] = 
				(from tf in GetComponentsInChildren<Transform>() where tf.gameObject.name == "RightFoot" select tf.gameObject).First();
			OffensiveObjects["LeftFoot"] = 
				(from tf in GetComponentsInChildren<Transform>() where tf.gameObject.name == "LeftFoot" select tf.gameObject).First();
			OffensiveObjects["LeftBall"] = 
				(from tf in GetComponentsInChildren<Transform>() where tf.gameObject.name == "LeftBall" select tf.gameObject).First();
			OffensiveObjects["RightBall"] = 
				(from tf in GetComponentsInChildren<Transform>() where tf.gameObject.name == "RightBall" select tf.gameObject).First();
			OffensiveObjects["Head"] = 
				(from tf in GetComponentsInChildren<Transform>() where tf.gameObject.name == "Head" select tf.gameObject).First();
		}

		protected virtual void Update()
		{
			if (state == null)
				throw new NullReferenceException("State is not initialized");

			state = state.Next();

			const float vmax = 5;

			Vector3 rbv = rb.velocity;
			rbv.x = rbv.x.Abs() > vmax ? vmax * rbv.x.Sign() : rbv.x;
			rb.velocity = rbv;

			animator.SetFloat("velocity", Mathf.Abs(rb.velocity.x));
			animator.SetBool("is_on_ground", IsGrounded());
			if (!IsVerticalDown()) animator.SetBool("isDown", false);

			if (orientation == Orientation.Left)
				transform.eulerAngles = Vector3.SmoothDamp(transform.eulerAngles, new Vector3(90, 90, 0), ref _turningVelocity, 0.1f);
			else 
				transform.eulerAngles = Vector3.SmoothDamp(transform.eulerAngles, new Vector3(90, 270, 0), ref _turningVelocity, 0.1f);
		}

		protected void OnTriggerEnter(Collider other)
		{
			if (other.CompareTag("PlayerHead") || other.CompareTag("PlayerBody"))
			{
				foreach (GameObject o in OffensiveObjects.Values) o.GetComponent<Collider>().enabled = false;

				Global.DealDamageTo(this.gameObject, other.gameObject, nextDamage);
			}
		}

		protected Vector3 GetInputDirection()
		{
			return new Vector3(Mathf.Abs(Input.GetAxis(Control.AxisX)) > 0.01 ? -Mathf.Sign(Input.GetAxis(Control.AxisX)) : 0,
				Mathf.Abs(Input.GetAxis(Control.AxisY)) > 0.01 ? Mathf.Sign(Input.GetAxis(Control.AxisY)) : 0,
				0);
		}

		protected GameObject FindChild(string name)
		{
			return (from tf in GetComponentsInChildren<Transform>() where tf.gameObject.name == name select tf.gameObject).First();
		}

		void AddForce(float up)
		{
			rb.AddForce(0,up,0,ForceMode.Impulse);
		}

		void StartChecking(string data)
		{
			(from tf in GetComponentsInChildren<Transform>() where tf.gameObject.name == data select tf.gameObject)
				.First().GetComponent<Collider>().enabled = true;
		}

		void StopChecking(string data)
		{
			(from tf in GetComponentsInChildren<Transform>() where tf.gameObject.name == data select tf.gameObject)
				.First().GetComponent<Collider>().enabled = false;
		}

		public bool IsGrounded()
		{
			return Physics.Raycast(transform.position + 0.2f*Vector3.up, -Vector3.up, 0.3f);
		}

		// conditions
		public bool NoCondition()
		{
			return true;
		}

		public bool IsHorizontal()
		{
			return Mathf.Abs(Input.GetAxis(Control.AxisX)) > 0.01;
		}

		public bool IsHorizontalTapped()
		{
			return Mathf.Abs(Input.GetAxis(Control.AxisX)) > 0.01 && Mathf.Abs(Input.GetAxis(Control.AxisX)) < 0.5;
		}

		public bool IsVertical()
		{
			return Mathf.Abs(Input.GetAxis(Control.AxisY)) > 0.01;
		}

		public bool IsVerticalDown()
		{
			return Input.GetAxis(Control.AxisY) < -0.01;
		}

		public bool IsVerticalUp()
		{
			return Input.GetAxis(Control.AxisY) > 0.01;
		}

		public bool IsADown()
		{
			return Input.GetButtonDown(Control.A);
		}

		public bool IsAPressed()
		{
			return Input.GetButton(Control.A);
		}

		public bool IsAUp()
		{
			return Input.GetButtonUp(Control.A) ^ (!Input.GetButton(Control.A));
		}

		public bool IsBDown()
		{
			return Input.GetButtonDown(Control.B);
		}

		public bool IsBPressed()
		{
			return Input.GetButton(Control.B);
		}

		public bool IsBUp()
		{
			return Input.GetButtonUp(Control.B) ^ (!Input.GetButton(Control.B));
		}

		public bool IsXDown()
		{
			return Input.GetButtonDown(Control.X);
		}

		public bool IsXPressed()
		{
			return Input.GetButton(Control.X);
		}

		public bool IsXUp()
		{
			return Input.GetButtonUp(Control.X) ^ (!Input.GetButton(Control.X));
		}

		public bool IsYDown()
		{
			return Input.GetButtonDown(Control.Y);
		}

		public bool IsYPressed()
		{
			return Input.GetButton(Control.Y);
		}

		public bool IsYUp()
		{
			return Input.GetButtonUp(Control.Y) ^ (!Input.GetButton(Control.Y));
		}

		public bool IsLDown()
		{
			return Input.GetButtonDown(Control.R);
		}

		public bool IsLPressed()
		{
			return Input.GetButton(Control.R);
		}

		public bool IsLUp()
		{
			return Input.GetButtonUp(Control.R) ^ (!Input.GetButton(Control.R));
		}

		public bool IsRDown()
		{
			return Input.GetButtonDown(Control.R);
		}

		public bool IsRPressed()
		{
			return Input.GetButton(Control.R);
		}

		public bool IsRUp()
		{
			return Input.GetButtonUp(Control.R) ^ (!Input.GetButton(Control.R));
		}

		public bool IsTimerPast(string name, string milliseconds)
		{
			float startTime;
			timers.TryGetValue(name, out startTime);
			return Time.time - startTime >= int.Parse(milliseconds) / 1000f;
		}

		public void StartTimer(string name)
		{
			//timers.Add(name, Time.time);
			timers[name] = Time.time;
		}

		public void MoveOnGround()
		{
			//orientation = Input.GetAxis(Control.AxisX) < 0 ? new Vector3(90, 90, 0) : new Vector3(90, 270, 0);
			orientation = Input.GetAxis(Control.AxisX) < 0 ? Orientation.Left : Orientation.Right;
			rb.AddForce(-20*Input.GetAxis(Control.AxisX), 0, 0);
		}

		public void MoveInAir()
		{
			if (Input.GetAxis(Control.AxisX).Abs() > 0.1)
				orientation = Input.GetAxis(Control.AxisX) < 0 ? Orientation.Left : Orientation.Right;
			rb.AddForce(-5*Input.GetAxis(Control.AxisX), 5*Input.GetAxis(Control.AxisY), 0);
		}

		public void LayDown()
		{
			animator.SetBool("isDown", true);
		}

		public void StandUp()
		{
			animator.SetBool("isDown", false);
		}
	}
}