﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using V = UnityEngine.Vector3;
using static U;
using System;
using static UnityEngine.Mathf;

public class CameraController : MonoBehaviour {
    public Vector3 followAxis = new Vector3(0, 0, 0);
    public Vector3 offset = new Vector3(0,0,0);
    public bool lookAt = true;
#if false
    // Use this for initialization
    void Start () {
        lookAt = false; // this should solve a bug.
	}
	
	// Update is called once per frame
	void Update () {
        if (followAxis == new Vector3(0, 0, 0)) {
            //transform.LookAt(PlayerController.that.transform);
            //transform.eulerAngles = new Vector3(0, transform.eulerAngles.y, 0);
        } else {
            if (lookAt) {
                transform.LookAt(PlayerController.that.transform);
                transform.eulerAngles = new Vector3(0, transform.eulerAngles.y, 0);
            }
            if (followAxis.x == 1) {
                //transform.position = new Vector3(PlayerController.that.transform.position.x, transform.position.y, transform.position.z);
                transform.aposition().x = PlayerController.that.transform.position.x;
            }
            if (followAxis.y == 1) {
                //transform.position = new Vector3(PlayerController.that.transform.position.x, transform.position.y, transform.position.z);
                transform.aposition().y = PlayerController.that.transform.position.y + offset.y;
            }
            if (followAxis.z == 1) {
                //transform.position = new Vector3(PlayerController.that.transform.position.x, transform.position.y, transform.position.z);
                transform.aposition().z = PlayerController.that.transform.position.z;
            }
        }
	}
#endif

    public static Func<Bounds, Bounds> DefaultStrategy = (bounds) => {
        bounds.max = V(bounds.max.x * 0.2f, 0);
        bounds.min = V(bounds.min.x * 0.2f, bounds.min.y * 0.7f);
        return bounds;
    };
    public Func<Bounds, Bounds> BoundsStrategy = DefaultStrategy;

    public enum CameraMode {
        Mario,
        Zelda,
        None
    }
    public CameraMode cameraMode;
    private void Update() {
        var player = PlayerController.that;
        switch (cameraMode) {
            case CameraMode.Mario:
                var viewbox = new Bounds() {
                    min = Camera.main.ViewportToWorldPoint(V(0, 0, Camera.main.transform.position.z.abs())),
                    max = Camera.main.ViewportToWorldPoint(V(1, 1, Camera.main.transform.position.z.abs()))
                };

                Bounds charBounds = viewbox;
                var oldCenter = charBounds.center;
                charBounds.center = V(0, 0);
                //print(charBounds.min + "" + charBounds.max);
                charBounds = BoundsStrategy(charBounds);
                //print(charBounds.min + "" + charBounds.max);
                charBounds.center += oldCenter;
                /*if (player.transform.position.x > charBounds.max.x) {
                    target.x += player.transform.position.x - charBounds.max.x;
                }
                if (player.transform.position.x < charBounds.min.x) {
                    target.x += player.transform.position.x - charBounds.min.x;
                }
                if (player.transform.position.y > charBounds.max.y) {
                    target.y += player.transform.position.y - charBounds.max.y;
                }
                if (player.transform.position.y < charBounds.min.y) {
                    target.y += player.transform.position.y - charBounds.min.y;
                }*/
                if (player.transform.position.x > charBounds.max.x) {
                    transform.aposition().x += player.transform.position.x - charBounds.max.x;
                }
                if (player.transform.position.x < charBounds.min.x) {
                    transform.aposition().x += player.transform.position.x - charBounds.min.x;
                }
                if (player.transform.position.y > charBounds.max.y) {
                    transform.aposition().y += player.transform.position.y - charBounds.max.y;
                }
                if (player.transform.position.y < charBounds.min.y) {
                    transform.aposition().y += player.transform.position.y - charBounds.min.y;
                }
                break;
            case CameraMode.Zelda:
                transform.position = player.transform.position + V(-8,+2.5f,0);
                transform.LookAt(player.transform);
                transform.eulerAngles = V(0, transform.eulerAngles.y, 0);
                break;
            case CameraMode.None:
                break;
        }

        Camera.main.transform.aposition().y = Max(Camera.main.transform.position.y, 1); // we force the camera in a certain area(maybe more complex stuff in the future)
    }
}
