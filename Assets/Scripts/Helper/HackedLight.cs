﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HackedLight : MonoBehaviour {
    private Vector4 hackColor;
    public float multiplier = 1;
    private new Light light;
    private AreaLight areaLight;

    private void Start() {
        light = this.GetLight();
        areaLight = GetComponent<AreaLight>();
        if (light) hackColor = light.color;
        if (areaLight) hackColor = areaLight.m_Color;
    }

    void Update() {
        if (light) light.color = new Color(hackColor.x, hackColor.y, hackColor.z, hackColor.w) * multiplier;
        if (areaLight) areaLight.m_Color = new Color(hackColor.x, hackColor.y, hackColor.z, hackColor.w) * multiplier;
    }
}
