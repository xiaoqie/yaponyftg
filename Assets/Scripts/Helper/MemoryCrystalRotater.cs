﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using static U;
using static UnityEngine.Mathf;
using V = UnityEngine.Vector3;
using CurveNS;

public class MemoryCrystalRotater : MonoBehaviour {
    private bool alreadyCollected;
    private float collectedTime;
    private void Update() {
        if (!alreadyCollected) {
            Cosine curve = Cosine.FromTo(0, 180, 1);
            transform.aeulerAngles().y += -curve.VelAt(Time.time % 1) * Time.deltaTime;
        } else {
            float ellapsedTime = Time.time - collectedTime;
            var mat = this.GetMeshRenderer().material;
            var c = mat.color; c.a = Lerp(1, 0, ellapsedTime); mat.color = c;

            Cosine curve = Cosine.FromTo(0, 180, 1);
            transform.aeulerAngles().y += -curve.VelAt(Time.time % 1) * Time.deltaTime * (ellapsedTime + 1)*3;

            if (ellapsedTime > 1) {
                gameObject.SetActive(false);
            }
        }
    }

    private void OnTriggerStay(Collider other) { // this is about rotating, but I don't bother rename this file.
        if (other.CompareTag("Player")) {
            if (!alreadyCollected) {
                // hey, you collected me!
                collectedTime = Time.time;
                alreadyCollected = true;
            }
        }
    }
}
