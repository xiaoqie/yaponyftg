﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using static U;
using static UnityEngine.Mathf;
using V = UnityEngine.Vector3;

public class RendererFade : Fade {
    private float time = 0.2f;
    private void Start() {
        time = 0;
        if (Default) {
            FadeIn();
        } else {
            FadeOut();
        }
        time = 0.2f; // so this is meaningless....
    }
    public override void FadeIn() {
        if (gameObject.activeInHierarchy) return;
        StopAllCoroutines();
        gameObject.SetActive(true);
        foreach (var renderer in GetComponentsInChildren<MeshRenderer>()) {
            var mat = renderer.material;
            StartCoroutine(LerpCoroutine((v) => { var c = mat.color; c.a = v; mat.color = c; }, mat.color.a, 1, time));
        }
    }

    public override void FadeOut() {
        if (!gameObject.activeInHierarchy) return;
        StopAllCoroutines();
        foreach (var renderer in GetComponentsInChildren<MeshRenderer>()) {
            var mat = renderer.material;
            StartCoroutine(LerpCoroutine((v) => { var c = mat.color; c.a = v; mat.color = c; }, mat.color.a, 0, time, final: () => { gameObject.SetActive(false); }));
        }
    }
}
