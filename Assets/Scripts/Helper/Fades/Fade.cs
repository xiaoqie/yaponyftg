﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using static U;
using static UnityEngine.Mathf;
using V = UnityEngine.Vector3;

public abstract class Fade : MonoBehaviour {
    public bool Default = false;
    public abstract void FadeIn();
    public abstract void FadeOut();
}
