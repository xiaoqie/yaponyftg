﻿using UnityEngine;

namespace Assets.Scripts
{
	public delegate void D_DelegateScript(DelegateScript self);
	public delegate void D_DelegateScript_Collider(DelegateScript self, Collider other);
	public delegate void D_DelegateScript_GameObject(DelegateScript self, GameObject other);

	public class DelegateScript : MonoBehaviour
	{

		public D_DelegateScript_Collider M_OnTriggerEnter;
		public D_DelegateScript_GameObject M_OnParticleCollision;
		public D_DelegateScript M_Start;
		public D_DelegateScript M_Update;

		// Use this for initialization
		void Start()
		{
			if (M_Start != null)
				M_Start(this);
		}

		// Update is called once per frame
		void Update()
		{
			if (M_Update != null)
				M_Update(this);
		}

		public void OnTriggerEnter(Collider other)
		{
			if (M_OnTriggerEnter != null)
				M_OnTriggerEnter(this, other);
		}

		// OnParticleCollision is called when a particle hits a collider
		public void OnParticleCollision(GameObject other)
		{
			if (M_OnParticleCollision != null)
				M_OnParticleCollision(this, other);
		}
	}
}
