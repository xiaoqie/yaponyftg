﻿using UnityEngine;
using System.Collections;

public class StartVelocity : MonoBehaviour {
    public Vector3 velocity;
    private Rigidbody rb;
    // Use this for initialization
    IEnumerator Start() {
        rb = this.GetRigidbody();
        rb.isKinematic = true;
        yield return new WaitForSeconds(1);
        rb.velocity = velocity;
        rb.isKinematic = false;
    }
}