﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using static U;
using static UnityEngine.Mathf;
using V = UnityEngine.Vector3;

public class FootstepSound : MonoBehaviour {
    private PlayerController player;
    private AudioSource audioSource;
    private void Start() {
        player = PlayerController.that;
        audioSource = this.GetAudioSource();
    }

    private void Update() {
        float v = player.GetMovingVelocity().abs() / PlayerController.vmax;
        if (!player.GetCharacterController().isGrounded) v = 0;
        audioSource.volume = v;
    }
}
