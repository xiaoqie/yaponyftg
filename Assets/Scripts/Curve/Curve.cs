﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace CurveNS {
    public abstract class CurveBase {
        public float EndTime = -1;
        public CurveBase EndAt(float t) {
            EndTime = t;
            return this;
        }
        public abstract float At(float t);
        public abstract float VelAt(float t);
    }
    public class Constant : CurveBase {
        private float value;

        public Constant(float v = 0) {
            value = v;
        }

        public override float At(float t) {
            return value;
        }

        public override float VelAt(float t) {
            return 0;
        }
    }
    public class Linear : CurveBase {
        private float slope;
        private float t0;

        public Linear(float slope, float t0 = 0) {
            this.slope = slope;
            this.t0 = t0;
        }

        public static Linear FromTo(float from, float to, float time) {
            return (Linear) new Linear((to - from) / time, from).EndAt(time);
        }

        public static Linear To(float to, float time) {
            return FromTo(0, to, time);
        }

        public override float At(float t) {
            return slope*t + t0;
        }

        public override float VelAt(float t) {
            return slope;
        }
    }
    public class Cosine : CurveBase {
        private float y1, y2;
        private float time;

        public Cosine(float y1, float y2, float time) {
            this.y1 = y1;
            this.y2 = y2;
            this.time = time;
        }

        public static Cosine FromTo(float from, float to, float time) {
            return (Cosine)new Cosine(from, to, time).EndAt(time);
        }

        public static Cosine To(float to, float time) {
            return FromTo(0, to, time);
        }

        public override float At(float mu) {
            mu /= time;

            float mu2 = (1 - Mathf.Cos(mu * Mathf.PI)) / 2;
            return (y1 * (1 - mu2) + y2 * mu2);
        }

        public override float VelAt(float mu) {
            mu /= time;

            float dmu2 = 1f / 2 * Mathf.PI * Mathf.Sin(Mathf.PI * mu)/time; // divide by time again, yay chain rule, hail chain rule
            return -y1*dmu2 + y2*dmu2;
        }
    }
    public class Curve : CurveBase {
        private Dictionary<float, CurveBase> starts = new Dictionary<float, CurveBase>();
        public Curve() {
            EndTime = 0;
        }
        public Curve Append(CurveBase curve) {
            if (curve.EndTime < 0) {
                throw new ArgumentException("curve do not end");
            }
            starts.Add(EndTime, curve);
            EndTime += curve.EndTime;
            return this;
        }

        public override float At(float t) {
            foreach (var pair in starts.OrderByDescending(x => x.Key)) {
                if (pair.Key < t) {
                    return pair.Value.At(t - pair.Key);
                }
            }
            return 0;
        }

        public override float VelAt(float t) {
            foreach (var pair in starts.OrderByDescending(x => x.Key)) {
                if (pair.Key < t) {
                    return pair.Value.VelAt(t - pair.Key);
                }
            }
            return 0;
        }
    }
}