﻿using UnityEngine;

namespace Assets.Scripts
{
	public class Damage
	{
		public enum Type
		{
			NO,
			STUN,
			LAUNCHER,
			KNOCK_DOWN,
			KNOCK_BACK,
		}

		public float damage = 1;

		public float horizontalForce = 0;
		public float verticalForce = 0;

		public float screenFrozenTime = 0.1f;

		public Type type = Type.STUN;

		public Vector3 forceHorizontalDirection = Vector3.zero;
	}
}