using System;
using System.Collections.Generic;

namespace Assets.Scripts.Automaton.Expr
{
	public class Call : Expr
	{
		public string Name;
		public List<string> Parameters = new List<string>();
		public CallExecutor Executor;

		public Call(string name)
		{
			Name = name;
		}

		public override string ToString()
		{
			return Name + "(" + String.Join(",", Parameters.ToArray()) + ")";
		}

		public override bool Execute()
		{
			return Executor(Name, Parameters.ToArray());
		}
	}
}