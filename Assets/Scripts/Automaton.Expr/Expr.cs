using System;
using Assets.Scripts.Iterator;

namespace Assets.Scripts.Automaton.Expr
{
	public delegate bool CallExecutor(string methodName, string[] parameters);

	public abstract class Expr
	{
		public static Expr Parse(ArrayIterator<char> it, CallExecutor callExecutor)
		{
			/* expr : expr OP expr (Op)
			 * expr : '!' expr (NON)
			 * expr : '[' expr ']'
			 * expr : token '(' paramlist ')' (Call)
			 */

			Expr expr1 = ParseSingleExpr(it, callExecutor);
			Expr expr2;
			string token = NextToken(it);
			switch (token)
			{
				case "&":
				case "*":
					expr2 = Parse(it, callExecutor);
					return new And(expr1, expr2);
				case "|":
					expr2 = Parse(it, callExecutor);
					return new Or(expr1, expr2);
				default:
					return expr1;
			}
		}

		public static Expr ParseSingleExpr(ArrayIterator<char> it, CallExecutor callExecutor)
		{
			string token = NextToken(it);
			Expr expr1 = null;
			switch (token)
			{
				case "[":
					expr1 = Parse(it, callExecutor);
					return expr1;
				case "!":
					expr1 = ParseSingleExpr(it, callExecutor);
					return new Not(expr1);
				default:
					expr1 = new Call(token);
					if (NextToken(it) != "(")
						throw new ArgumentException("no '(' after a call");
					while ((token = NextToken(it)) != ")")
					{
						((Call) expr1).Parameters.Add(token);
					}
					((Call) expr1).Executor = callExecutor;
					return expr1;
			}
		}

		public static string NextToken(ArrayIterator<char> it)
		{
			if (!it.HasNext()) return "";

			string token = "";
			while (it.Current() == ' ' || it.Current() == '\t')
				if (!it.MoveNext()) return "";

			if (Char.IsDigit(it.Current()))
			{
				do
				{
					token += it.Current();
					if (!it.MoveNext()) break;
				} while (Char.IsDigit(it.Current()));
			}
			else if (Char.IsLetter(it.Current()) || it.Current() == '_')
			{
				do
				{
					token += it.Current();
					if (!it.MoveNext()) break;
				} while (Char.IsLetterOrDigit(it.Current()) || it.Current() == '_');
			}
			else
			{
				token += it.Current();
				it.MoveNext();
			}
			return token;
		}

		public abstract bool Execute();
	}
}