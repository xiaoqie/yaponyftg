namespace Assets.Scripts.Automaton.Expr
{
	public class And : Expr
	{
		public Expr Expr1;
		public Expr Expr2;

		public And(Expr expr1, Expr expr2)
		{
			Expr1 = expr1;
			Expr2 = expr2;
		}

		public override string ToString()
		{
			return "[" + Expr1 + "&" + Expr2 + "]";
		}

		public override bool Execute()
		{
			return Expr1.Execute() & Expr2.Execute();
		}
	}
}