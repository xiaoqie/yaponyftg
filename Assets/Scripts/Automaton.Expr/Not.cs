namespace Assets.Scripts.Automaton.Expr
{
	public class Not : Expr
	{
		public Expr Expr1;

		public Not(Expr expr1)
		{
			Expr1 = expr1;
		}

		public override string ToString()
		{
			return "[!" + Expr1 + "]";
		}

		public override bool Execute()
		{
			return !Expr1.Execute();
		}
	}
}