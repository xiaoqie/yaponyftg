﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static U;
using V = UnityEngine.Vector3;

public class FixLocalPosition : MonoBehaviour {
    private V initLocalPosition;
	// Use this for initialization
	void Start () {
        initLocalPosition = transform.localPosition;
	}
	
	// Update is called once per frame
	void Update () {
        transform.localPosition = initLocalPosition;
	}
}
