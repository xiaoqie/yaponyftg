﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using V = UnityEngine.Vector3;

public static class U {
    public const float E = 0.1f;
    public const float E2 = 0.01f;
    public static readonly V V0 = new V(0, 0, 0);
    public static readonly Vector3 V3NAN = new Vector3(float.NaN, float.NaN, float.NaN);
    public static bool IsNaN(this Vector3 self) {
        return float.IsNaN(self.x) && float.IsNaN(self.y) && float.IsNaN(self.z);
    }
	public static Vector3 V(float x, float y, float z) {
		return new Vector3(x, y, z);
	}
	public static Vector3 V(float x, float y) {
		return new Vector3(x, y);
	}
	public static bool TryGet(this Dictionary<string, bool> dict, string key) {
        bool ret;
        dict.TryGetValue(key, out ret);
        return ret;
    }
	public static T Cached<T>(Func<T> t) {
		return default(T);
	}
    public static int sign(this int v) {
        return (int)Mathf.Sign(v);
    }
    public static float sign(this float v) {
        return Mathf.Sign(v);
    }
    public static int RoughSign(this int v) {
        return v.abs() > 0.01 ? v.sign() : 0;
    }
    public static float RoughSign(this float v) {
        return v.abs() > 0.01 ? v.sign() : 0;
    }
    public static int abs(this int v) {
        return Mathf.Abs(v);
    }
    public static float abs(this float v) {
        return Mathf.Abs(v);
    }
    public static float StdDev(this IEnumerable<float> values) {
        float ret = 0;
        int count = values.Count();
        if (count > 1) {
            //Compute the Average
            float avg = values.Average();

            //Perform the Sum of (value-avg)^2
            float sum = values.Sum(d => (d - avg) * (d - avg));

            //Put it all together
            ret = Mathf.Sqrt(sum / count);
        }
        return ret;
    }
    public static float Tanh(float x) {
        return (Mathf.Exp(x) - Mathf.Exp(-x)) / (Mathf.Exp(x) + Mathf.Exp(-x));
    }
    public static bool HasFlag(this Enum variable, Enum value) {
        if (variable == null)
            return false;

        if (value == null)
            throw new ArgumentNullException("value");

        // Not as good as the .NET 4 version of this function, but should be good enough
        if (!Enum.IsDefined(variable.GetType(), value)) {
            throw new ArgumentException(string.Format(
                "Enumeration type mismatch.  The flag is of type '{0}', was expecting '{1}'.",
                value.GetType(), variable.GetType()));
        }

        ulong num = Convert.ToUInt64(value);
        return ((Convert.ToUInt64(variable) & num) == num);

    }

    public static float RoundToPixel(this float unityUnits) {
        float valueInPixels = (Screen.height / (Camera.main.orthographicSize * 2)) * unityUnits;
        valueInPixels = Mathf.Round(valueInPixels);
        float adjustedUnityUnits = valueInPixels / (Screen.height / (Camera.main.orthographicSize * 2));
        return adjustedUnityUnits;
    }

    /*public static void PlaySoundEffect(this GameObject @this, string name) {
        AudioSource a = @this.GetComponent<AudioSource>();
        if (!a)
            a = @this.AddComponent<AudioSource>();
        a.PlayOneShot(SoundManager.sounds[name]);
    }*/

    public static bool ContainBounds(this Bounds bounds, Bounds target) {
        return bounds.Contains(target.min) && bounds.Contains(target.max);
    }

    public static IEnumerator WaitForSecondsCoroutine(float t, Action a) {
        yield return new WaitForSeconds(t);
        a();
    }

    public static IEnumerator LerpCoroutine(Action<float> changeTo, float start, float end, float time, Action final = null) {
        float startTime = Time.time;
        while ((Time.time - startTime) < time) {
            changeTo(Mathf.Lerp(start, end, (Time.time - startTime)/time));
            yield return 0;
        }
        changeTo(end);

        final?.Invoke();
    }

    public static IEnumerator LerpCoroutine(Action<V> changeTo, V start, V end, float time, Action final = null) {
        float startTime = Time.time;
        while ((Time.time - startTime) < time) {
            changeTo(Vector3.Lerp(start, end, (Time.time - startTime) / time));
            yield return 0;
        }
        changeTo(end);

        final?.Invoke();
    }

    public static Collider2D CheckDeath(GameObject go) {
        Collider2D self = go.GetComponent<Collider2D>();
        Bounds dmg = go.GetComponent<Collider2D>().bounds;
        Collider2D[] cols = Physics2D.OverlapBoxAll(dmg.center, dmg.size, 0, LayerMask.GetMask("Die"));
        return cols.FirstOrDefault((col) => col != self);
        /*if (cols.Any((col) => col != self))
        {
            return true;
        }
        return false;*/
    }
    
    public static Bounds Bounds(this Camera camera) {
        float screenAspect = (float)Screen.width / (float)Screen.height;
        float cameraHeight = camera.orthographicSize * 2;
        Bounds bounds = new Bounds(
            (Vector2)camera.transform.position,
            new Vector2(cameraHeight * screenAspect, cameraHeight));
        return bounds;
    }

    public static Bounds RendererBounds(this GameObject go) {
        return go.GetComponent<Renderer>().bounds;
    }

    public static Vector3 Elemwise(this Vector3 v, Func<float, float> op) {
        return new Vector3(op(v.x), op(v.y), op(v.z));
    }
    
    public static PositionAgent aposition(this Transform t) {
        return new PositionAgent(t);
    }
    public static EulerAngleAgent aeulerAngles(this Transform t) {
        return new EulerAngleAgent(t);
    }
    public static LocalScaleAgent alocalScale(this Transform t) {
        return new LocalScaleAgent(t);
    }

    public class PositionAgent {
        Transform transform;
        public PositionAgent(Transform t) {
            transform = t;
        }
        public float x {
            set {
                Vector3 v = transform.position;
                v.x = value;
                transform.position = v;
            }
            get {
                return transform.position.x;
            }

        }
        public float y {
            set {
                Vector3 v = transform.position;
                v.y = value;
                transform.position = v;
            }
            get {
                return transform.position.y;
            }
        }
        public float z {
            set {
                Vector3 v = transform.position;
                v.z = value;
                transform.position = v;
            }
            get {
                return transform.position.z;
            }
        }
    }

    public class EulerAngleAgent {
        Transform transform;
        public EulerAngleAgent(Transform t) {
            transform = t;
        }
        public float x {
            set {
                Vector3 v = transform.eulerAngles;
                v.x = value;
                transform.eulerAngles = v;
            }

            get {
                return transform.eulerAngles.x;
            }
        }
        public float y {
            set {
                Vector3 v = transform.eulerAngles;
                v.y = value;
                transform.eulerAngles = v;
            }
            get {
                return transform.eulerAngles.y;
            }
        }
        public float z {
            set {
                Vector3 v = transform.eulerAngles;
                v.z = value;
                transform.eulerAngles = v;
            }
            get {
                return transform.eulerAngles.z;
            }
        }
    }

    public class LocalScaleAgent {
        Transform transform;
        public LocalScaleAgent(Transform t) {
            transform = t;
        }
        public float x {
            set {
                Vector3 v = transform.localScale;
                v.x = value;
                transform.localScale = v;
            }
            get {
                return transform.localScale.x;
            }
        }
        public float y {
            set {
                Vector3 v = transform.localScale;
                v.y = value;
                transform.localScale = v;
            }
            get {
                return transform.localScale.y;
            }
        }
        public float z {
            set {
                Vector3 v = transform.localScale;
                v.z = value;
                transform.localScale = v;
            }
            get {
                return transform.localScale.z;
            }
        }
    }
}

public static class LayerU {
    public static readonly int Tile;
    public static readonly int Default;
    public static readonly int Die;
    public static readonly int Character;
    public static readonly int Trigger;
    public static readonly int OneWayUp;
    public static readonly int Coin;
    static LayerU() {
        Tile = LayerMask.NameToLayer("Tile");
        Default = LayerMask.NameToLayer("Default");
        Die = LayerMask.NameToLayer("Die");
        Character = LayerMask.NameToLayer("Character");
        Trigger = LayerMask.NameToLayer("Trigger");
        OneWayUp = LayerMask.NameToLayer("OneWayUp");
        Coin = LayerMask.NameToLayer("Coin");
    }
}

public static class LayerMaskU {
    public static int Everything {
        get {
            return ~0;
        }
    }
    public static int Nothing {
        get {
            return 0;
        }
    }
    public static bool ContainsLayer(this int layermask, int layer) {
        if ((layermask & 1 << layer) != 0)
            return true;
        return false;
    }
}