﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using static U;
using static UnityEngine.Mathf;
using V = UnityEngine.Vector3;

public class HOPEController : MonoBehaviour {
    public static HOPEController that { get { return FindObjectOfType<HOPEController>(); } }
    ParticleSystem[] particles;

    private void Start() {
        particles = GetComponentsInChildren<ParticleSystem>();
    }

    public void Play(MeshRenderer mesh) {
        foreach (var particle in particles) {
            var shape = particle.shape;
            shape.meshRenderer = mesh;
            //var main = particle.main;
            //main.startSize = 1;
            particle.Play();
        }
    }

    public void Stop() {
        foreach (var particle in particles) {
            particle.Stop();
        }
    }
}
