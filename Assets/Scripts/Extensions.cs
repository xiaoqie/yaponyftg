﻿using System.Linq;
using UnityEngine;

namespace Assets.Scripts
{
	public static class Extensions
	{
		public static int Sign(this int v)
		{
			return (int) Mathf.Sign(v);
		}
		public static float Sign(this float v)
		{
			return Mathf.Sign(v);
		}
		public static int RoughSign(this int v)
		{
			return v.Abs() > 0.01 ? v.Sign() : 0;
		}
		public static float RoughSign(this float v)
		{
			return v.Abs() > 0.01 ? v.Sign() : 0;
		}

		public static int Abs(this int v)
		{
			return Mathf.Abs(v);
		}
		public static float Abs(this float v)
		{
			return Mathf.Abs(v);
		}
	}
}