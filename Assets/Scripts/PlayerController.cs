﻿using CurveNS;
using System;
using System.Collections;
using System.Diagnostics;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using static U;
using static UnityEngine.Mathf;
using State = System.Action<bool>;
using V = UnityEngine.Vector3;


public class PlayerController : MonoBehaviour {
    [HideInInspector] public bool safe = false;


    Animator animator;
    CharacterController characterController;
    //Rigidbody rb;
    private V _turningVelocity;
    private V _neckTurningVelocity;
    private int orientation = 1;
    public Transform neck;
    private V targetNeck;

    public SkinnedMeshRenderer horn;
    //public LineRenderer lineRenderer;
    public HOPEController HOPE;
    public ParticleSystem hornParticle;
    private Transform hornLight;

    /**
     * when false:
     *       ^ Y
     *       |
     *   < -- -- > X
     *       |
     * when true:
     *       ^ X
     *       |
     *   < -- -- > Y
     *       |
     * (moving axis)
     */
    private bool transposeAxis = false;
    public bool movable = true;

    public bool meetingRoomMode;

	public string cameraMode = "Default";

    private V pos;
    private V posLast;
    private V vel;
    private V acc;
    public const float vmax = 5;
    public float GetMovingVelocity() => vel.x;

    private float animatorVelocity;
    private float animatorVelocity_maxSpeed;

    private V targetRight;
    private void SetTargetRight(V right) {
        targetRight = right;
    }
    private void SetTargetForward(V forward) { 
        V original = transform.right;
        transform.forward = forward;
        targetRight = transform.right;
        //transform.right = original; // it dosen't matter if we set it back, so... disable it if you like
    }
	private void SetTargetUp(V up) {
		V original = transform.right;
		transform.up = up;
		targetRight = transform.right;
		//transform.right = original; // it dosen't matter if we set it back, so... disable it if you like
	}

	public static PlayerController that { get { return FindObjectOfType<PlayerController>(); } }

	private State state;
	private float enterTime;
	// Use this for initialization
	void Start() {
        //Destroy(that); // this temporarily solves "PlayerController.that doesn't point to this" problem. but don't do this in release.
        
        HOPE = HOPEController.that;
        characterController = this.GetCharacterController();
        animator = GetComponentInChildren<Animator>();
        hornLight = transform.Find("TwilightSparkle/vn_twilightsparkle_horn/HornLight");
        hornLight.gameObject.SetActive(false);
        //rb = GetComponent<Rigidbody>();
        state = MeetingRoomWalk;
        aimingState = NotAiming;
        targetNeck = neck.eulerAngles;
        pos = posLast = transform.position;

        print(cameraMode);
		SetCameraMode(cameraMode);
        //#if UNITY_EDITOR
#if false
		string code = @"
using UnityEngine;

public static class GetComponentExtensions {
";
		foreach (var type in from assembly in AppDomain.CurrentDomain.GetAssemblies()
						  from type in assembly.GetTypes()
						  where type.IsSubclassOf(typeof(MonoBehaviour)) || type.IsSubclassOf(typeof(Component)) || type.IsSubclassOf(typeof(Behaviour))
						  select type) {
			string name = type.Name;
			string fullname = type.FullName;

			bool obsolete = false;
			foreach (ObsoleteAttribute attribute in type.GetCustomAttributes(false).OfType<ObsoleteAttribute>())
				obsolete = true;
			if (obsolete) continue;
			if (type.IsNotPublic) continue;
			if (type.IsNested) continue;
			if (type.Namespace == "UnityEditor") continue;
			//if (type.Namespace != null && type.Namespace != "UnityEngine" && type.Namespace.Contains("Unity")) continue;

			code += $@"
	public static {fullname} Get{name}(this GameObject self) => self.GetComponent<{fullname}>();
	public static {fullname} Get{name}(this Component self) => self.GetComponent<{fullname}>();
";
		}
		code += "}";
		string filename = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(new StackTrace(true).GetFrame(0).GetFileName()), "GetComponentExtensions.cs");
		System.IO.File.WriteAllText(filename, code);
#endif
        /*
		float t0 = Time.realtimeSinceStartup;
		Camera c = Camera.main;
		var d = Camera.main.GetComponent<CameraController>();
		for (int i = 0; i < 100000; i++) {
			//c.GetComponent<CameraController>().lookAt = false;
			c.GetComponent<Rigidbody>();
			//d.lookAt = false;
		}
		print(Time.realtimeSinceStartup - t0);*/
    }

	void Update() {
		//GameObject.Find("TopLeftDebugText").GetComponent<Text>().text = state.Method.Name;

		V lastRight = transform.right;
		state(false);
		transform.right = lastRight;
		Quaternion rightRot = Quaternion.FromToRotation(lastRight, targetRight);
		transform.aeulerAngles().y = LerpAngle(transform.eulerAngles.y, transform.eulerAngles.y + rightRot.eulerAngles.y, 0.1f);

		posLast = pos;
		pos = transform.position;
        //float target = (posLast - pos).magnitude / Time.deltaTime;
        float target = (posLast - pos).x.abs() / Time.deltaTime; // this breaks meeting room walk
        if (target == 0) {// oh, you hit a wall
			vel.x = 0;
		}
		animatorVelocity = Mathf.SmoothDamp(animatorVelocity, target, ref animatorVelocity_maxSpeed, 0.1f);
		animator.SetFloat("velocity", animatorVelocity);

        //animator.enabled = characterController.isGrounded; // TODO: better jump down animation
        animator.SetBool("is_on_ground", characterController.isGrounded);
        if (!characterController.isGrounded) animator.SetFloat("JumpVel", -(vel.y) / 10);

        if (!characterController.isGrounded) {
            groundedOn?.GetLiftable()?.StopLifting();
            groundedOn = null;
        }
        groundedOn?.GetLiftable()?.TurnRed();

        hornLight.position = horn.bounds.center + transform.right*0.3f + transform.up * 0.3f;

        if (characterController.isGrounded && Input.GetKeyDown(KeyCode.Space) && movable) {
            vel.y = 10;
            animator.Play("onGround"); // if the player is in jumping transition state, force it to onGround, so the animator automatically transiste it to jumping state
        }
        //print(vel.y);

        transform.aposition().z = -0.6f;
    }

	void FixedUpdate() {
		state(true);
	}

    public void SetCameraMode() {
        SetCameraMode(cameraMode);
    }

	private void SetCameraMode(string mode) {
		/*switch (s) {
			case 0: // track low
				state = Walk;
				moveAlongAxis = V(1, 0, 0);
				Camera.main.transform.position = transform.position + V(0, 0, -8);
				foreach (Camera c in Camera.main.GetComponentsInChildren<Camera>()) {
					//c.orthographic = true;
				}
				break;

			case 1: // track high
				state = Walk;
				moveAlongAxis = V(1, 0, 0);
				Camera.main.transform.position = transform.position + V(0, 0, -8);
				foreach (Camera c in Camera.main.GetComponentsInChildren<Camera>()) {
					c.orthographic = true;
					c.orthographicSize *= 1.5f;
				}
				break;

			case 2: // don't track on Y
				state = Walk;
				moveAlongAxis = V(1, 0, 0);
				Camera.main.transform.position = transform.position + V(0, 6f, -8);
				foreach (Camera c in Camera.main.GetComponentsInChildren<Camera>()) {
					c.orthographic = true;
					c.orthographicSize *= 2f;
				}
				break;

			default:
				break;
		}*/
        switch (mode) {
            case "Default":
                state = Walk;
                moveAlongAxis = V(1, 0, 0);


                Camera.main.GetCameraController().cameraMode = CameraController.CameraMode.Mario;

                // i',m sleepy right now, i don;t know why i excatly commented below
                //Camera.main.transform.position = transform.position + V(0, 0, -8);
                //Camera.main.transform.eulerAngles = V(0, 0, 0);

                transposeAxis = false;
                break;
            case "Forward":
                state = Walk;
                moveAlongAxis = V(1, 0, 0);

                transposeAxis = true;

                Camera.main.GetCameraController().cameraMode = CameraController.CameraMode.Zelda;
                break;
            case "None":
                break;
            default:
                throw new ArgumentException($"Bad camera mode:{mode}");
        }
    }
    
    public float walkAcc = 10;
    public float stopDeacc = 4;

    private Interactive currentInteractive;
    private void OnTriggerEnter(Collider other) {
		if (!other.isTrigger) return;
        GlowObject go = other.GetComponentInChildren<GlowObject>();
        if (go) {
            go.Glow();
            currentInteractive = other.GetComponentInChildren<Interactive>();
        }
    }
    private void OnTriggerExit(Collider other) {
		if (!other.isTrigger) return;
		GlowObject go = other.GetComponentInChildren<GlowObject>();
        if (go) {
            go.Unglow();
            currentInteractive = null;
        }
    }
    GameObject groundedOn;
    private void OnControllerColliderHit(ControllerColliderHit hit) {
        //SetTargetUp(hit.normal);
        hit.gameObject.GetSwitch()?.PushDown();
        if (hit.point.y < gameObject.transform.position.y + 0.5f) {// magical constant 0.5f
            groundedOn?.GetLiftable()?.StopLifting(); // null conditioning is super dooper fun

            groundedOn = hit.gameObject;
            if (hit.gameObject.GetLiftable()?.gameObject == lifting) { // wtf is this, null conditioning is super fun huh?
                forceStopLift = true;
                //whoIsRed = hit.gameObject.GetLiftable();
                //whoIsRed.TurnRed();
            }
        }
        if (hit.point.y > gameObject.transform.position.y + 1f) {// magical constant 0.5f
            if (vel.y > 0) vel.y = 0;
        }
    }

    public void Die() {
        //transform.GetChild(0).gameObject.SetActive(false);
        //StartCoroutine((Func<IEnumerator>)(() => { yield return 0; }));
        Camera.main.GetGrayscale().enabled = true;

        enabled = false;
        animator.enabled = false;
        const float timeToDie = 0.5f;
        var light = hornLight.GetLight();
        StartCoroutine(LerpCoroutine((v) => { light.intensity = v; }, light.intensity, 0, timeToDie));

        StartCoroutine(WaitForSecondsCoroutine(2, () => UnityEngine.SceneManagement.SceneManager.LoadScene(0)));
        //StartCoroutine(WaitForSecondsCoroutine(timeToDie + 0.1f, () => { gameObject.SetActive(false); }));
    }

    private float? forcedHorizontalAxis = null;
    public void ForceHorizontalInput(float? horizontal) => forcedHorizontalAxis = horizontal;

    private float GetHorizontalInput() {
        if (forcedHorizontalAxis.HasValue) return forcedHorizontalAxis.Value;
        return Input.GetAxis(transposeAxis ? "p1y1" : "p1x1");
    }

	#region WaitAndGoto
	private float secondWaitingFor = 0;
    private State gotoState;
    private void WaitAndGoto(float sec, State s) {
        secondWaitingFor = sec;
        gotoState = s;
        Goto(WaitForSec);
    }
    private void WaitForSec(bool isFixedUpdate) {
        if (isFixedUpdate) {
            return;
        }
        if (Time.time - enterTime > secondWaitingFor) {
            Goto(gotoState);
            gotoState = null;
        }
    }
    private void Goto(State act) {
        state = act;
        enterTime = Time.time;
    }
#endregion
    private void UpdateInternalPhysics() {
        float horizontal = GetHorizontalInput();
        if (!movable) horizontal = 0;
        orientation = horizontal.abs() < 0.1f ? orientation : (int)horizontal;

        //rb.AddForce(horizontal * new Vector2(20, 0));

        acc += horizontal * V(walkAcc, 0);
        if (horizontal.abs() < 0.1f)
            acc += -vel.x * V(stopDeacc, 0);

        vel += acc * Time.fixedDeltaTime;

        acc = V.zero;
    }
    private void MeetingRoomWalk(bool isFixedUpdate) {
        if (isFixedUpdate) {
            UpdateInternalPhysics();
            return;
        }

        vel.x = vel.x.abs() > vmax ? vmax * vel.x.sign() : vel.x;

        float dist = transform.position.magnitude;
        GetComponent<CharacterController>().SimpleMove(vel.x*transform.right*orientation);
        SetTargetForward(orientation/*orientation method is temporary*/*transform.position.normalized); // simple math
        transform.position = dist * transform.position.normalized;//keep distance to origin fixed

        if (currentInteractive) {
            if (currentInteractive is OpenableDoor) {
                if (Input.GetKeyDown(KeyCode.Space)) {
                    currentInteractive.Do();
					SetTargetForward(V(1,0,0));
                    WaitAndGoto(1, EnterHallway(currentInteractive as OpenableDoor));
                }
            }
        }
    }

    private State EnterHallway(OpenableDoor door) {
        float moveToCenterDist = (door.transform.position - transform.position).x;
        Curve cameraX = new Curve().Append(Cosine.To(-1, time: 2.2f)).Append(Cosine.FromTo(-1, -8 + moveToCenterDist, time: 0.8f));
        Curve cameraZ = new Curve().Append(Linear.To(-15 - 7.0572f, time: 3));
        return (isFixedUpdate) => {
            if (isFixedUpdate) { return; }
            vel.x = 0;
            GetComponent<CharacterController>().SimpleMove(V(moveToCenterDist/3, 0, -5));
            Camera.main.transform.position += V(
                x: cameraX.VelAt(Time.time - enterTime),
                y: 0,
                z: cameraZ.VelAt(Time.time - enterTime)
                )*Time.deltaTime; // too slow, very inefficient
            if (Time.time - enterTime > 3f) {
                orientation = 1;
                moveAlongAxis = -V(0, 0, 1); // TODO: where does the minus sign come from?
                Camera.main.GetComponent<CameraController>().followAxis = V(0, 0, 1);
                Camera.main.GetComponent<CameraController>().lookAt = false;
                door.Close();
                Goto(Walk);
            }
        };
    }

    private State EnterLevel(OpenableDoor door) {
        float moveToCenterDist = (door.transform.position - transform.position).z;
        Curve cameraZ = new Curve().Append(Cosine.To(-1, time: 1.5f)).Append(Cosine.FromTo(-1, -6 + moveToCenterDist, time: 0.5f));
        Curve cameraX = new Curve().Append(Linear.To(18, time: 2));
        return (isFixedUpdate) => {
            if (isFixedUpdate) { return; }
            vel.x = 0;
            GetComponent<CharacterController>().SimpleMove(V(5, 0, moveToCenterDist/2));
            Camera.main.GetComponent<CameraController>().followAxis = V(0, 0, 0);
            Camera.main.transform.position += V(
                x: cameraX.VelAt(Time.time - enterTime),
                y: 0,
                z: cameraZ.VelAt(Time.time - enterTime)
                ) * Time.deltaTime; // too slow, very inefficient
            if (Time.time - enterTime > 2f) {
                orientation = 1;
                moveAlongAxis = -V(-1, 0, 0); // TODO: where does the minus sign come from?
                Camera.main.GetComponent<CameraController>().followAxis = V(1, 1, 0);
                Camera.main.GetComponent<CameraController>().offset = V(0, 2.5f, 0);
                //Camera.main.GetComponent<CameraController>().lookAt = true;
                door.Close();
                Goto(Walk);
            }
        };
    }
	
    private Action aimingState;
    private float aimingStateEnterTime;
    private void GotoAimingState(Action s) {
        aimingState = s;
        aimingStateEnterTime = Time.time;
    }
    private void NotAiming() {
		if (Input.GetButton("Fire")) {
			RaycastHit hit;
			if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, Mathf.Infinity, LayerMask.GetMask("Liftable"), QueryTriggerInteraction.Ignore)) {
				hornParticle.Play();

				lifting = hit.collider.gameObject;

				V original = lifting.transform.position;
				V mouse = Camera.main.ScreenToWorldPoint(V(Input.mousePosition.x, Input.mousePosition.y, lifting.transform.position.z - Camera.main.transform.position.z));
				lifting.transform.parent.position = mouse;
				lifting.transform.position = original;

				GotoAimingState(Lift);

				lifting.GetLiftable().StartLifting();
                HOPE.Play(lifting.GetLiftable().GetQindingMeshRenderer());
			}
			if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, Mathf.Infinity, LayerMask.GetMask("Switch"), QueryTriggerInteraction.Collide)) {
				hornParticle.Play();
				lifting = hit.collider.transform.parent.gameObject;
				GotoAimingState(PushDownSwitch);

                HOPE.Play(lifting.GetMeshRenderer());
            }
		}
    }
    GameObject lifting;
	bool forceStopLift; // when stand on it
    V lastVel = V3NAN;
    private void Lift() {
        V mouse = Camera.main.ScreenToWorldPoint(V(Input.mousePosition.x, Input.mousePosition.y, lifting.transform.position.z - Camera.main.transform.position.z));
        Rigidbody liftRB = lifting.transform.parent.GetComponent<Rigidbody>();

        if (liftRB.mass < 5 || Approximately(liftRB.mass, 1000)) { // arbitrarily liftable, 1000 to simulate unmovable object, is this good?
            V diff = mouse - lifting.transform.parent.position;
            V newVel = diff.normalized * Tanh(diff.magnitude) * 10;
            liftRB.velocity = newVel;
            if (lastVel.IsNaN()) lastVel = newVel;
            V acc = (liftRB.velocity - lastVel) / Time.deltaTime;
            liftRB.angularVelocity += V.Cross(lifting.transform.parent.position - lifting.transform.position, V(0, 10, 0)/*gravity*/ + acc) * Time.deltaTime; // perhaps the right way to add torque
            // liftRB.angularVelocity *= 0.95f; // is this the right way to add damping? i forget about it. (I just used unity's angular dump)
            lastVel = newVel;
        } else { // hard to lift
            liftRB.AddForce(60 * (mouse - lifting.transform.parent.position).normalized);
        }

        if (!Input.GetButton("Fire") || forceStopLift) {
            HOPE.Stop();
			lifting.GetComponent<Liftable>().StopLifting();
			forceStopLift = false;
			hornParticle.Stop();
            animator.enabled = true;
            GotoAimingState(NotAiming);
        }
    }
	private void PushDownSwitch() {
		lifting.GetSwitch().PushDown();
		if (!Input.GetButton("Fire")) {
			HOPE.Stop();
			hornParticle.Stop();
			animator.enabled = true;
			GotoAimingState(NotAiming);
		}
	}
	private V moveAlongAxis;
	private bool isGrounded;
    private void Walk(bool isFixedUpdate) { // FIXME: you can stop turning by aiming
        if (isFixedUpdate) {
            UpdateInternalPhysics();
            return;
        }

        const float vmax = 5;
        vel.x = vel.x.abs() > vmax ? vmax * vel.x.sign() : vel.x;

		//GetComponent<CharacterController>().SimpleMove(vel.x * moveAlongAxis);
		V direction = vel.x * moveAlongAxis;
		var charController = this.GetCharacterController();
		if (charController.isGrounded) {
			if (vel.y < 5) vel.y = -6;
			isGrounded = true;
		} else {
			if (isGrounded /* last frame */) {
				vel.y += 4; // -6 + 4 = -2, because we introduced jumping, we can't just set it to -2
			}
            float deacc = 40;
            if (Input.GetKey(KeyCode.Space) && vel.y > 0)
                deacc = 20;
			vel.y -= deacc * Time.deltaTime; // we do mario style.
			vel.y = Clamp(vel.y, -10, 10);
			isGrounded = false;
		}
        var groundedRb = groundedOn?.transform.parent?.GetRigidbody();
        V relativeVel = V(0, 0);
        if (characterController.isGrounded && groundedRb) {
            relativeVel = groundedRb.velocity;
        }
		charController.Move((direction + V(0, vel.y, 0) + relativeVel) * Time.deltaTime);

		V targetRight = moveAlongAxis * orientation;
        SetTargetRight(targetRight);

        aimingState();
        
        if (currentInteractive) {
            if (currentInteractive is OpenableDoor) {
                if (Input.GetKeyDown(KeyCode.Space)) {
                    currentInteractive.Do();
                    SetTargetForward(V(0, 0, 1));
                    WaitAndGoto(1, EnterLevel(currentInteractive as OpenableDoor));
                }
            }
            if (currentInteractive is ReadableBook) {
                //print("do something that seems you read");
            }
        }
        /*RaycastHit[] hits = Physics.RaycastAll(transform.position + 0.2f * V.up, -V.up, 0.3f);
        foreach (var hit in hits) {
            if (hit.normal == V.up) break;
            transform.up = hit.normal;
            break;
        }*/
    }
}