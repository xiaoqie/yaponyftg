
using UnityEngine;

public static class GetComponentExtensions {

	public static UnityEngine.Behaviour GetBehaviour(this GameObject self) => self.GetComponent<UnityEngine.Behaviour>();
	public static UnityEngine.Behaviour GetBehaviour(this Component self) => self.GetComponent<UnityEngine.Behaviour>();

	public static UnityEngine.BillboardRenderer GetBillboardRenderer(this GameObject self) => self.GetComponent<UnityEngine.BillboardRenderer>();
	public static UnityEngine.BillboardRenderer GetBillboardRenderer(this Component self) => self.GetComponent<UnityEngine.BillboardRenderer>();

	public static UnityEngine.Camera GetCamera(this GameObject self) => self.GetComponent<UnityEngine.Camera>();
	public static UnityEngine.Camera GetCamera(this Component self) => self.GetComponent<UnityEngine.Camera>();

	public static UnityEngine.FlareLayer GetFlareLayer(this GameObject self) => self.GetComponent<UnityEngine.FlareLayer>();
	public static UnityEngine.FlareLayer GetFlareLayer(this Component self) => self.GetComponent<UnityEngine.FlareLayer>();

	public static UnityEngine.OcclusionArea GetOcclusionArea(this GameObject self) => self.GetComponent<UnityEngine.OcclusionArea>();
	public static UnityEngine.OcclusionArea GetOcclusionArea(this Component self) => self.GetComponent<UnityEngine.OcclusionArea>();

	public static UnityEngine.OcclusionPortal GetOcclusionPortal(this GameObject self) => self.GetComponent<UnityEngine.OcclusionPortal>();
	public static UnityEngine.OcclusionPortal GetOcclusionPortal(this Component self) => self.GetComponent<UnityEngine.OcclusionPortal>();

	public static UnityEngine.MeshFilter GetMeshFilter(this GameObject self) => self.GetComponent<UnityEngine.MeshFilter>();
	public static UnityEngine.MeshFilter GetMeshFilter(this Component self) => self.GetComponent<UnityEngine.MeshFilter>();

	public static UnityEngine.SkinnedMeshRenderer GetSkinnedMeshRenderer(this GameObject self) => self.GetComponent<UnityEngine.SkinnedMeshRenderer>();
	public static UnityEngine.SkinnedMeshRenderer GetSkinnedMeshRenderer(this Component self) => self.GetComponent<UnityEngine.SkinnedMeshRenderer>();

	public static UnityEngine.LensFlare GetLensFlare(this GameObject self) => self.GetComponent<UnityEngine.LensFlare>();
	public static UnityEngine.LensFlare GetLensFlare(this Component self) => self.GetComponent<UnityEngine.LensFlare>();

	public static UnityEngine.Renderer GetRenderer(this GameObject self) => self.GetComponent<UnityEngine.Renderer>();
	public static UnityEngine.Renderer GetRenderer(this Component self) => self.GetComponent<UnityEngine.Renderer>();

	public static UnityEngine.Projector GetProjector(this GameObject self) => self.GetComponent<UnityEngine.Projector>();
	public static UnityEngine.Projector GetProjector(this Component self) => self.GetComponent<UnityEngine.Projector>();

	public static UnityEngine.Skybox GetSkybox(this GameObject self) => self.GetComponent<UnityEngine.Skybox>();
	public static UnityEngine.Skybox GetSkybox(this Component self) => self.GetComponent<UnityEngine.Skybox>();

	public static UnityEngine.TrailRenderer GetTrailRenderer(this GameObject self) => self.GetComponent<UnityEngine.TrailRenderer>();
	public static UnityEngine.TrailRenderer GetTrailRenderer(this Component self) => self.GetComponent<UnityEngine.TrailRenderer>();

	public static UnityEngine.LineRenderer GetLineRenderer(this GameObject self) => self.GetComponent<UnityEngine.LineRenderer>();
	public static UnityEngine.LineRenderer GetLineRenderer(this Component self) => self.GetComponent<UnityEngine.LineRenderer>();

	public static UnityEngine.MeshRenderer GetMeshRenderer(this GameObject self) => self.GetComponent<UnityEngine.MeshRenderer>();
	public static UnityEngine.MeshRenderer GetMeshRenderer(this Component self) => self.GetComponent<UnityEngine.MeshRenderer>();

	public static UnityEngine.GUIElement GetGUIElement(this GameObject self) => self.GetComponent<UnityEngine.GUIElement>();
	public static UnityEngine.GUIElement GetGUIElement(this Component self) => self.GetComponent<UnityEngine.GUIElement>();

	public static UnityEngine.GUITexture GetGUITexture(this GameObject self) => self.GetComponent<UnityEngine.GUITexture>();
	public static UnityEngine.GUITexture GetGUITexture(this Component self) => self.GetComponent<UnityEngine.GUITexture>();

	public static UnityEngine.GUILayer GetGUILayer(this GameObject self) => self.GetComponent<UnityEngine.GUILayer>();
	public static UnityEngine.GUILayer GetGUILayer(this Component self) => self.GetComponent<UnityEngine.GUILayer>();

	public static UnityEngine.Light GetLight(this GameObject self) => self.GetComponent<UnityEngine.Light>();
	public static UnityEngine.Light GetLight(this Component self) => self.GetComponent<UnityEngine.Light>();

	public static UnityEngine.LightProbeGroup GetLightProbeGroup(this GameObject self) => self.GetComponent<UnityEngine.LightProbeGroup>();
	public static UnityEngine.LightProbeGroup GetLightProbeGroup(this Component self) => self.GetComponent<UnityEngine.LightProbeGroup>();

	public static UnityEngine.LightProbeProxyVolume GetLightProbeProxyVolume(this GameObject self) => self.GetComponent<UnityEngine.LightProbeProxyVolume>();
	public static UnityEngine.LightProbeProxyVolume GetLightProbeProxyVolume(this Component self) => self.GetComponent<UnityEngine.LightProbeProxyVolume>();

	public static UnityEngine.LODGroup GetLODGroup(this GameObject self) => self.GetComponent<UnityEngine.LODGroup>();
	public static UnityEngine.LODGroup GetLODGroup(this Component self) => self.GetComponent<UnityEngine.LODGroup>();

	public static UnityEngine.MonoBehaviour GetMonoBehaviour(this GameObject self) => self.GetComponent<UnityEngine.MonoBehaviour>();
	public static UnityEngine.MonoBehaviour GetMonoBehaviour(this Component self) => self.GetComponent<UnityEngine.MonoBehaviour>();

	public static UnityEngine.ReflectionProbe GetReflectionProbe(this GameObject self) => self.GetComponent<UnityEngine.ReflectionProbe>();
	public static UnityEngine.ReflectionProbe GetReflectionProbe(this Component self) => self.GetComponent<UnityEngine.ReflectionProbe>();

	public static UnityEngine.SpriteRenderer GetSpriteRenderer(this GameObject self) => self.GetComponent<UnityEngine.SpriteRenderer>();
	public static UnityEngine.SpriteRenderer GetSpriteRenderer(this Component self) => self.GetComponent<UnityEngine.SpriteRenderer>();

	public static UnityEngine.WindZone GetWindZone(this GameObject self) => self.GetComponent<UnityEngine.WindZone>();
	public static UnityEngine.WindZone GetWindZone(this Component self) => self.GetComponent<UnityEngine.WindZone>();

	public static UnityEngine.Transform GetTransform(this GameObject self) => self.GetComponent<UnityEngine.Transform>();
	public static UnityEngine.Transform GetTransform(this Component self) => self.GetComponent<UnityEngine.Transform>();

	public static UnityEngine.RectTransform GetRectTransform(this GameObject self) => self.GetComponent<UnityEngine.RectTransform>();
	public static UnityEngine.RectTransform GetRectTransform(this Component self) => self.GetComponent<UnityEngine.RectTransform>();

	public static UnityEngine.Rendering.SortingGroup GetSortingGroup(this GameObject self) => self.GetComponent<UnityEngine.Rendering.SortingGroup>();
	public static UnityEngine.Rendering.SortingGroup GetSortingGroup(this Component self) => self.GetComponent<UnityEngine.Rendering.SortingGroup>();

	public static UnityEngine.ParticleSystem GetParticleSystem(this GameObject self) => self.GetComponent<UnityEngine.ParticleSystem>();
	public static UnityEngine.ParticleSystem GetParticleSystem(this Component self) => self.GetComponent<UnityEngine.ParticleSystem>();

	public static UnityEngine.ParticleSystemRenderer GetParticleSystemRenderer(this GameObject self) => self.GetComponent<UnityEngine.ParticleSystemRenderer>();
	public static UnityEngine.ParticleSystemRenderer GetParticleSystemRenderer(this Component self) => self.GetComponent<UnityEngine.ParticleSystemRenderer>();

	public static UnityEngine.Rigidbody GetRigidbody(this GameObject self) => self.GetComponent<UnityEngine.Rigidbody>();
	public static UnityEngine.Rigidbody GetRigidbody(this Component self) => self.GetComponent<UnityEngine.Rigidbody>();

	public static UnityEngine.Joint GetJoint(this GameObject self) => self.GetComponent<UnityEngine.Joint>();
	public static UnityEngine.Joint GetJoint(this Component self) => self.GetComponent<UnityEngine.Joint>();

	public static UnityEngine.HingeJoint GetHingeJoint(this GameObject self) => self.GetComponent<UnityEngine.HingeJoint>();
	public static UnityEngine.HingeJoint GetHingeJoint(this Component self) => self.GetComponent<UnityEngine.HingeJoint>();

	public static UnityEngine.SpringJoint GetSpringJoint(this GameObject self) => self.GetComponent<UnityEngine.SpringJoint>();
	public static UnityEngine.SpringJoint GetSpringJoint(this Component self) => self.GetComponent<UnityEngine.SpringJoint>();

	public static UnityEngine.FixedJoint GetFixedJoint(this GameObject self) => self.GetComponent<UnityEngine.FixedJoint>();
	public static UnityEngine.FixedJoint GetFixedJoint(this Component self) => self.GetComponent<UnityEngine.FixedJoint>();

	public static UnityEngine.CharacterJoint GetCharacterJoint(this GameObject self) => self.GetComponent<UnityEngine.CharacterJoint>();
	public static UnityEngine.CharacterJoint GetCharacterJoint(this Component self) => self.GetComponent<UnityEngine.CharacterJoint>();

	public static UnityEngine.ConfigurableJoint GetConfigurableJoint(this GameObject self) => self.GetComponent<UnityEngine.ConfigurableJoint>();
	public static UnityEngine.ConfigurableJoint GetConfigurableJoint(this Component self) => self.GetComponent<UnityEngine.ConfigurableJoint>();

	public static UnityEngine.ConstantForce GetConstantForce(this GameObject self) => self.GetComponent<UnityEngine.ConstantForce>();
	public static UnityEngine.ConstantForce GetConstantForce(this Component self) => self.GetComponent<UnityEngine.ConstantForce>();

	public static UnityEngine.Collider GetCollider(this GameObject self) => self.GetComponent<UnityEngine.Collider>();
	public static UnityEngine.Collider GetCollider(this Component self) => self.GetComponent<UnityEngine.Collider>();

	public static UnityEngine.BoxCollider GetBoxCollider(this GameObject self) => self.GetComponent<UnityEngine.BoxCollider>();
	public static UnityEngine.BoxCollider GetBoxCollider(this Component self) => self.GetComponent<UnityEngine.BoxCollider>();

	public static UnityEngine.SphereCollider GetSphereCollider(this GameObject self) => self.GetComponent<UnityEngine.SphereCollider>();
	public static UnityEngine.SphereCollider GetSphereCollider(this Component self) => self.GetComponent<UnityEngine.SphereCollider>();

	public static UnityEngine.MeshCollider GetMeshCollider(this GameObject self) => self.GetComponent<UnityEngine.MeshCollider>();
	public static UnityEngine.MeshCollider GetMeshCollider(this Component self) => self.GetComponent<UnityEngine.MeshCollider>();

	public static UnityEngine.CapsuleCollider GetCapsuleCollider(this GameObject self) => self.GetComponent<UnityEngine.CapsuleCollider>();
	public static UnityEngine.CapsuleCollider GetCapsuleCollider(this Component self) => self.GetComponent<UnityEngine.CapsuleCollider>();

	public static UnityEngine.CharacterController GetCharacterController(this GameObject self) => self.GetComponent<UnityEngine.CharacterController>();
	public static UnityEngine.CharacterController GetCharacterController(this Component self) => self.GetComponent<UnityEngine.CharacterController>();

	public static UnityEngine.CircleCollider2D GetCircleCollider2D(this GameObject self) => self.GetComponent<UnityEngine.CircleCollider2D>();
	public static UnityEngine.CircleCollider2D GetCircleCollider2D(this Component self) => self.GetComponent<UnityEngine.CircleCollider2D>();

	public static UnityEngine.BoxCollider2D GetBoxCollider2D(this GameObject self) => self.GetComponent<UnityEngine.BoxCollider2D>();
	public static UnityEngine.BoxCollider2D GetBoxCollider2D(this Component self) => self.GetComponent<UnityEngine.BoxCollider2D>();

	public static UnityEngine.Joint2D GetJoint2D(this GameObject self) => self.GetComponent<UnityEngine.Joint2D>();
	public static UnityEngine.Joint2D GetJoint2D(this Component self) => self.GetComponent<UnityEngine.Joint2D>();

	public static UnityEngine.AreaEffector2D GetAreaEffector2D(this GameObject self) => self.GetComponent<UnityEngine.AreaEffector2D>();
	public static UnityEngine.AreaEffector2D GetAreaEffector2D(this Component self) => self.GetComponent<UnityEngine.AreaEffector2D>();

	public static UnityEngine.PlatformEffector2D GetPlatformEffector2D(this GameObject self) => self.GetComponent<UnityEngine.PlatformEffector2D>();
	public static UnityEngine.PlatformEffector2D GetPlatformEffector2D(this Component self) => self.GetComponent<UnityEngine.PlatformEffector2D>();

	public static UnityEngine.Rigidbody2D GetRigidbody2D(this GameObject self) => self.GetComponent<UnityEngine.Rigidbody2D>();
	public static UnityEngine.Rigidbody2D GetRigidbody2D(this Component self) => self.GetComponent<UnityEngine.Rigidbody2D>();

	public static UnityEngine.Collider2D GetCollider2D(this GameObject self) => self.GetComponent<UnityEngine.Collider2D>();
	public static UnityEngine.Collider2D GetCollider2D(this Component self) => self.GetComponent<UnityEngine.Collider2D>();

	public static UnityEngine.EdgeCollider2D GetEdgeCollider2D(this GameObject self) => self.GetComponent<UnityEngine.EdgeCollider2D>();
	public static UnityEngine.EdgeCollider2D GetEdgeCollider2D(this Component self) => self.GetComponent<UnityEngine.EdgeCollider2D>();

	public static UnityEngine.CapsuleCollider2D GetCapsuleCollider2D(this GameObject self) => self.GetComponent<UnityEngine.CapsuleCollider2D>();
	public static UnityEngine.CapsuleCollider2D GetCapsuleCollider2D(this Component self) => self.GetComponent<UnityEngine.CapsuleCollider2D>();

	public static UnityEngine.CompositeCollider2D GetCompositeCollider2D(this GameObject self) => self.GetComponent<UnityEngine.CompositeCollider2D>();
	public static UnityEngine.CompositeCollider2D GetCompositeCollider2D(this Component self) => self.GetComponent<UnityEngine.CompositeCollider2D>();

	public static UnityEngine.PolygonCollider2D GetPolygonCollider2D(this GameObject self) => self.GetComponent<UnityEngine.PolygonCollider2D>();
	public static UnityEngine.PolygonCollider2D GetPolygonCollider2D(this Component self) => self.GetComponent<UnityEngine.PolygonCollider2D>();

	public static UnityEngine.AnchoredJoint2D GetAnchoredJoint2D(this GameObject self) => self.GetComponent<UnityEngine.AnchoredJoint2D>();
	public static UnityEngine.AnchoredJoint2D GetAnchoredJoint2D(this Component self) => self.GetComponent<UnityEngine.AnchoredJoint2D>();

	public static UnityEngine.SpringJoint2D GetSpringJoint2D(this GameObject self) => self.GetComponent<UnityEngine.SpringJoint2D>();
	public static UnityEngine.SpringJoint2D GetSpringJoint2D(this Component self) => self.GetComponent<UnityEngine.SpringJoint2D>();

	public static UnityEngine.DistanceJoint2D GetDistanceJoint2D(this GameObject self) => self.GetComponent<UnityEngine.DistanceJoint2D>();
	public static UnityEngine.DistanceJoint2D GetDistanceJoint2D(this Component self) => self.GetComponent<UnityEngine.DistanceJoint2D>();

	public static UnityEngine.FrictionJoint2D GetFrictionJoint2D(this GameObject self) => self.GetComponent<UnityEngine.FrictionJoint2D>();
	public static UnityEngine.FrictionJoint2D GetFrictionJoint2D(this Component self) => self.GetComponent<UnityEngine.FrictionJoint2D>();

	public static UnityEngine.HingeJoint2D GetHingeJoint2D(this GameObject self) => self.GetComponent<UnityEngine.HingeJoint2D>();
	public static UnityEngine.HingeJoint2D GetHingeJoint2D(this Component self) => self.GetComponent<UnityEngine.HingeJoint2D>();

	public static UnityEngine.RelativeJoint2D GetRelativeJoint2D(this GameObject self) => self.GetComponent<UnityEngine.RelativeJoint2D>();
	public static UnityEngine.RelativeJoint2D GetRelativeJoint2D(this Component self) => self.GetComponent<UnityEngine.RelativeJoint2D>();

	public static UnityEngine.SliderJoint2D GetSliderJoint2D(this GameObject self) => self.GetComponent<UnityEngine.SliderJoint2D>();
	public static UnityEngine.SliderJoint2D GetSliderJoint2D(this Component self) => self.GetComponent<UnityEngine.SliderJoint2D>();

	public static UnityEngine.TargetJoint2D GetTargetJoint2D(this GameObject self) => self.GetComponent<UnityEngine.TargetJoint2D>();
	public static UnityEngine.TargetJoint2D GetTargetJoint2D(this Component self) => self.GetComponent<UnityEngine.TargetJoint2D>();

	public static UnityEngine.FixedJoint2D GetFixedJoint2D(this GameObject self) => self.GetComponent<UnityEngine.FixedJoint2D>();
	public static UnityEngine.FixedJoint2D GetFixedJoint2D(this Component self) => self.GetComponent<UnityEngine.FixedJoint2D>();

	public static UnityEngine.WheelJoint2D GetWheelJoint2D(this GameObject self) => self.GetComponent<UnityEngine.WheelJoint2D>();
	public static UnityEngine.WheelJoint2D GetWheelJoint2D(this Component self) => self.GetComponent<UnityEngine.WheelJoint2D>();

	public static UnityEngine.PhysicsUpdateBehaviour2D GetPhysicsUpdateBehaviour2D(this GameObject self) => self.GetComponent<UnityEngine.PhysicsUpdateBehaviour2D>();
	public static UnityEngine.PhysicsUpdateBehaviour2D GetPhysicsUpdateBehaviour2D(this Component self) => self.GetComponent<UnityEngine.PhysicsUpdateBehaviour2D>();

	public static UnityEngine.ConstantForce2D GetConstantForce2D(this GameObject self) => self.GetComponent<UnityEngine.ConstantForce2D>();
	public static UnityEngine.ConstantForce2D GetConstantForce2D(this Component self) => self.GetComponent<UnityEngine.ConstantForce2D>();

	public static UnityEngine.Effector2D GetEffector2D(this GameObject self) => self.GetComponent<UnityEngine.Effector2D>();
	public static UnityEngine.Effector2D GetEffector2D(this Component self) => self.GetComponent<UnityEngine.Effector2D>();

	public static UnityEngine.BuoyancyEffector2D GetBuoyancyEffector2D(this GameObject self) => self.GetComponent<UnityEngine.BuoyancyEffector2D>();
	public static UnityEngine.BuoyancyEffector2D GetBuoyancyEffector2D(this Component self) => self.GetComponent<UnityEngine.BuoyancyEffector2D>();

	public static UnityEngine.PointEffector2D GetPointEffector2D(this GameObject self) => self.GetComponent<UnityEngine.PointEffector2D>();
	public static UnityEngine.PointEffector2D GetPointEffector2D(this Component self) => self.GetComponent<UnityEngine.PointEffector2D>();

	public static UnityEngine.SurfaceEffector2D GetSurfaceEffector2D(this GameObject self) => self.GetComponent<UnityEngine.SurfaceEffector2D>();
	public static UnityEngine.SurfaceEffector2D GetSurfaceEffector2D(this Component self) => self.GetComponent<UnityEngine.SurfaceEffector2D>();

	public static UnityEngine.WheelCollider GetWheelCollider(this GameObject self) => self.GetComponent<UnityEngine.WheelCollider>();
	public static UnityEngine.WheelCollider GetWheelCollider(this Component self) => self.GetComponent<UnityEngine.WheelCollider>();

	public static UnityEngine.Cloth GetCloth(this GameObject self) => self.GetComponent<UnityEngine.Cloth>();
	public static UnityEngine.Cloth GetCloth(this Component self) => self.GetComponent<UnityEngine.Cloth>();

	public static UnityEngine.AI.NavMeshAgent GetNavMeshAgent(this GameObject self) => self.GetComponent<UnityEngine.AI.NavMeshAgent>();
	public static UnityEngine.AI.NavMeshAgent GetNavMeshAgent(this Component self) => self.GetComponent<UnityEngine.AI.NavMeshAgent>();

	public static UnityEngine.AI.NavMeshObstacle GetNavMeshObstacle(this GameObject self) => self.GetComponent<UnityEngine.AI.NavMeshObstacle>();
	public static UnityEngine.AI.NavMeshObstacle GetNavMeshObstacle(this Component self) => self.GetComponent<UnityEngine.AI.NavMeshObstacle>();

	public static UnityEngine.AI.OffMeshLink GetOffMeshLink(this GameObject self) => self.GetComponent<UnityEngine.AI.OffMeshLink>();
	public static UnityEngine.AI.OffMeshLink GetOffMeshLink(this Component self) => self.GetComponent<UnityEngine.AI.OffMeshLink>();

	public static UnityEngine.AudioSource GetAudioSource(this GameObject self) => self.GetComponent<UnityEngine.AudioSource>();
	public static UnityEngine.AudioSource GetAudioSource(this Component self) => self.GetComponent<UnityEngine.AudioSource>();

	public static UnityEngine.AudioLowPassFilter GetAudioLowPassFilter(this GameObject self) => self.GetComponent<UnityEngine.AudioLowPassFilter>();
	public static UnityEngine.AudioLowPassFilter GetAudioLowPassFilter(this Component self) => self.GetComponent<UnityEngine.AudioLowPassFilter>();

	public static UnityEngine.AudioHighPassFilter GetAudioHighPassFilter(this GameObject self) => self.GetComponent<UnityEngine.AudioHighPassFilter>();
	public static UnityEngine.AudioHighPassFilter GetAudioHighPassFilter(this Component self) => self.GetComponent<UnityEngine.AudioHighPassFilter>();

	public static UnityEngine.AudioReverbFilter GetAudioReverbFilter(this GameObject self) => self.GetComponent<UnityEngine.AudioReverbFilter>();
	public static UnityEngine.AudioReverbFilter GetAudioReverbFilter(this Component self) => self.GetComponent<UnityEngine.AudioReverbFilter>();

	public static UnityEngine.AudioBehaviour GetAudioBehaviour(this GameObject self) => self.GetComponent<UnityEngine.AudioBehaviour>();
	public static UnityEngine.AudioBehaviour GetAudioBehaviour(this Component self) => self.GetComponent<UnityEngine.AudioBehaviour>();

	public static UnityEngine.AudioListener GetAudioListener(this GameObject self) => self.GetComponent<UnityEngine.AudioListener>();
	public static UnityEngine.AudioListener GetAudioListener(this Component self) => self.GetComponent<UnityEngine.AudioListener>();

	public static UnityEngine.AudioReverbZone GetAudioReverbZone(this GameObject self) => self.GetComponent<UnityEngine.AudioReverbZone>();
	public static UnityEngine.AudioReverbZone GetAudioReverbZone(this Component self) => self.GetComponent<UnityEngine.AudioReverbZone>();

	public static UnityEngine.AudioDistortionFilter GetAudioDistortionFilter(this GameObject self) => self.GetComponent<UnityEngine.AudioDistortionFilter>();
	public static UnityEngine.AudioDistortionFilter GetAudioDistortionFilter(this Component self) => self.GetComponent<UnityEngine.AudioDistortionFilter>();

	public static UnityEngine.AudioEchoFilter GetAudioEchoFilter(this GameObject self) => self.GetComponent<UnityEngine.AudioEchoFilter>();
	public static UnityEngine.AudioEchoFilter GetAudioEchoFilter(this Component self) => self.GetComponent<UnityEngine.AudioEchoFilter>();

	public static UnityEngine.AudioChorusFilter GetAudioChorusFilter(this GameObject self) => self.GetComponent<UnityEngine.AudioChorusFilter>();
	public static UnityEngine.AudioChorusFilter GetAudioChorusFilter(this Component self) => self.GetComponent<UnityEngine.AudioChorusFilter>();

	public static UnityEngine.Animator GetAnimator(this GameObject self) => self.GetComponent<UnityEngine.Animator>();
	public static UnityEngine.Animator GetAnimator(this Component self) => self.GetComponent<UnityEngine.Animator>();

	public static UnityEngine.Animation GetAnimation(this GameObject self) => self.GetComponent<UnityEngine.Animation>();
	public static UnityEngine.Animation GetAnimation(this Component self) => self.GetComponent<UnityEngine.Animation>();

	public static UnityEngine.Terrain GetTerrain(this GameObject self) => self.GetComponent<UnityEngine.Terrain>();
	public static UnityEngine.Terrain GetTerrain(this Component self) => self.GetComponent<UnityEngine.Terrain>();

	public static UnityEngine.Tree GetTree(this GameObject self) => self.GetComponent<UnityEngine.Tree>();
	public static UnityEngine.Tree GetTree(this Component self) => self.GetComponent<UnityEngine.Tree>();

	public static UnityEngine.GUIText GetGUIText(this GameObject self) => self.GetComponent<UnityEngine.GUIText>();
	public static UnityEngine.GUIText GetGUIText(this Component self) => self.GetComponent<UnityEngine.GUIText>();

	public static UnityEngine.TextMesh GetTextMesh(this GameObject self) => self.GetComponent<UnityEngine.TextMesh>();
	public static UnityEngine.TextMesh GetTextMesh(this Component self) => self.GetComponent<UnityEngine.TextMesh>();

	public static UnityEngine.Canvas GetCanvas(this GameObject self) => self.GetComponent<UnityEngine.Canvas>();
	public static UnityEngine.Canvas GetCanvas(this Component self) => self.GetComponent<UnityEngine.Canvas>();

	public static UnityEngine.CanvasGroup GetCanvasGroup(this GameObject self) => self.GetComponent<UnityEngine.CanvasGroup>();
	public static UnityEngine.CanvasGroup GetCanvasGroup(this Component self) => self.GetComponent<UnityEngine.CanvasGroup>();

	public static UnityEngine.CanvasRenderer GetCanvasRenderer(this GameObject self) => self.GetComponent<UnityEngine.CanvasRenderer>();
	public static UnityEngine.CanvasRenderer GetCanvasRenderer(this Component self) => self.GetComponent<UnityEngine.CanvasRenderer>();

	public static UnityEngine.TerrainCollider GetTerrainCollider(this GameObject self) => self.GetComponent<UnityEngine.TerrainCollider>();
	public static UnityEngine.TerrainCollider GetTerrainCollider(this Component self) => self.GetComponent<UnityEngine.TerrainCollider>();

	public static UnityEngine.Networking.Match.NetworkMatch GetNetworkMatch(this GameObject self) => self.GetComponent<UnityEngine.Networking.Match.NetworkMatch>();
	public static UnityEngine.Networking.Match.NetworkMatch GetNetworkMatch(this Component self) => self.GetComponent<UnityEngine.Networking.Match.NetworkMatch>();

	public static UnityEngine.Playables.PlayableDirector GetPlayableDirector(this GameObject self) => self.GetComponent<UnityEngine.Playables.PlayableDirector>();
	public static UnityEngine.Playables.PlayableDirector GetPlayableDirector(this Component self) => self.GetComponent<UnityEngine.Playables.PlayableDirector>();

	public static UnityEngine.Video.VideoPlayer GetVideoPlayer(this GameObject self) => self.GetComponent<UnityEngine.Video.VideoPlayer>();
	public static UnityEngine.Video.VideoPlayer GetVideoPlayer(this Component self) => self.GetComponent<UnityEngine.Video.VideoPlayer>();

	public static UnityEngine.SpriteMask GetSpriteMask(this GameObject self) => self.GetComponent<UnityEngine.SpriteMask>();
	public static UnityEngine.SpriteMask GetSpriteMask(this Component self) => self.GetComponent<UnityEngine.SpriteMask>();

	public static UnityStandardAssets.ImageEffects.Antialiasing GetAntialiasing(this GameObject self) => self.GetComponent<UnityStandardAssets.ImageEffects.Antialiasing>();
	public static UnityStandardAssets.ImageEffects.Antialiasing GetAntialiasing(this Component self) => self.GetComponent<UnityStandardAssets.ImageEffects.Antialiasing>();

	public static UnityStandardAssets.ImageEffects.Bloom GetBloom(this GameObject self) => self.GetComponent<UnityStandardAssets.ImageEffects.Bloom>();
	public static UnityStandardAssets.ImageEffects.Bloom GetBloom(this Component self) => self.GetComponent<UnityStandardAssets.ImageEffects.Bloom>();

	public static UnityStandardAssets.ImageEffects.BloomAndFlares GetBloomAndFlares(this GameObject self) => self.GetComponent<UnityStandardAssets.ImageEffects.BloomAndFlares>();
	public static UnityStandardAssets.ImageEffects.BloomAndFlares GetBloomAndFlares(this Component self) => self.GetComponent<UnityStandardAssets.ImageEffects.BloomAndFlares>();

	public static UnityStandardAssets.ImageEffects.BloomOptimized GetBloomOptimized(this GameObject self) => self.GetComponent<UnityStandardAssets.ImageEffects.BloomOptimized>();
	public static UnityStandardAssets.ImageEffects.BloomOptimized GetBloomOptimized(this Component self) => self.GetComponent<UnityStandardAssets.ImageEffects.BloomOptimized>();

	public static UnityStandardAssets.ImageEffects.Blur GetBlur(this GameObject self) => self.GetComponent<UnityStandardAssets.ImageEffects.Blur>();
	public static UnityStandardAssets.ImageEffects.Blur GetBlur(this Component self) => self.GetComponent<UnityStandardAssets.ImageEffects.Blur>();

	public static UnityStandardAssets.ImageEffects.BlurOptimized GetBlurOptimized(this GameObject self) => self.GetComponent<UnityStandardAssets.ImageEffects.BlurOptimized>();
	public static UnityStandardAssets.ImageEffects.BlurOptimized GetBlurOptimized(this Component self) => self.GetComponent<UnityStandardAssets.ImageEffects.BlurOptimized>();

	public static UnityStandardAssets.ImageEffects.CameraMotionBlur GetCameraMotionBlur(this GameObject self) => self.GetComponent<UnityStandardAssets.ImageEffects.CameraMotionBlur>();
	public static UnityStandardAssets.ImageEffects.CameraMotionBlur GetCameraMotionBlur(this Component self) => self.GetComponent<UnityStandardAssets.ImageEffects.CameraMotionBlur>();

	public static UnityStandardAssets.ImageEffects.ColorCorrectionCurves GetColorCorrectionCurves(this GameObject self) => self.GetComponent<UnityStandardAssets.ImageEffects.ColorCorrectionCurves>();
	public static UnityStandardAssets.ImageEffects.ColorCorrectionCurves GetColorCorrectionCurves(this Component self) => self.GetComponent<UnityStandardAssets.ImageEffects.ColorCorrectionCurves>();

	public static UnityStandardAssets.ImageEffects.ColorCorrectionLookup GetColorCorrectionLookup(this GameObject self) => self.GetComponent<UnityStandardAssets.ImageEffects.ColorCorrectionLookup>();
	public static UnityStandardAssets.ImageEffects.ColorCorrectionLookup GetColorCorrectionLookup(this Component self) => self.GetComponent<UnityStandardAssets.ImageEffects.ColorCorrectionLookup>();

	public static UnityStandardAssets.ImageEffects.ColorCorrectionRamp GetColorCorrectionRamp(this GameObject self) => self.GetComponent<UnityStandardAssets.ImageEffects.ColorCorrectionRamp>();
	public static UnityStandardAssets.ImageEffects.ColorCorrectionRamp GetColorCorrectionRamp(this Component self) => self.GetComponent<UnityStandardAssets.ImageEffects.ColorCorrectionRamp>();

	public static UnityStandardAssets.ImageEffects.ContrastEnhance GetContrastEnhance(this GameObject self) => self.GetComponent<UnityStandardAssets.ImageEffects.ContrastEnhance>();
	public static UnityStandardAssets.ImageEffects.ContrastEnhance GetContrastEnhance(this Component self) => self.GetComponent<UnityStandardAssets.ImageEffects.ContrastEnhance>();

	public static UnityStandardAssets.ImageEffects.ContrastStretch GetContrastStretch(this GameObject self) => self.GetComponent<UnityStandardAssets.ImageEffects.ContrastStretch>();
	public static UnityStandardAssets.ImageEffects.ContrastStretch GetContrastStretch(this Component self) => self.GetComponent<UnityStandardAssets.ImageEffects.ContrastStretch>();

	public static UnityStandardAssets.ImageEffects.CreaseShading GetCreaseShading(this GameObject self) => self.GetComponent<UnityStandardAssets.ImageEffects.CreaseShading>();
	public static UnityStandardAssets.ImageEffects.CreaseShading GetCreaseShading(this Component self) => self.GetComponent<UnityStandardAssets.ImageEffects.CreaseShading>();

	public static UnityStandardAssets.ImageEffects.DepthOfField GetDepthOfField(this GameObject self) => self.GetComponent<UnityStandardAssets.ImageEffects.DepthOfField>();
	public static UnityStandardAssets.ImageEffects.DepthOfField GetDepthOfField(this Component self) => self.GetComponent<UnityStandardAssets.ImageEffects.DepthOfField>();

	public static UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated GetDepthOfFieldDeprecated(this GameObject self) => self.GetComponent<UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated>();
	public static UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated GetDepthOfFieldDeprecated(this Component self) => self.GetComponent<UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated>();

	public static UnityStandardAssets.ImageEffects.EdgeDetection GetEdgeDetection(this GameObject self) => self.GetComponent<UnityStandardAssets.ImageEffects.EdgeDetection>();
	public static UnityStandardAssets.ImageEffects.EdgeDetection GetEdgeDetection(this Component self) => self.GetComponent<UnityStandardAssets.ImageEffects.EdgeDetection>();

	public static UnityStandardAssets.ImageEffects.Fisheye GetFisheye(this GameObject self) => self.GetComponent<UnityStandardAssets.ImageEffects.Fisheye>();
	public static UnityStandardAssets.ImageEffects.Fisheye GetFisheye(this Component self) => self.GetComponent<UnityStandardAssets.ImageEffects.Fisheye>();

	public static UnityStandardAssets.ImageEffects.Grayscale GetGrayscale(this GameObject self) => self.GetComponent<UnityStandardAssets.ImageEffects.Grayscale>();
	public static UnityStandardAssets.ImageEffects.Grayscale GetGrayscale(this Component self) => self.GetComponent<UnityStandardAssets.ImageEffects.Grayscale>();

	public static UnityStandardAssets.ImageEffects.ImageEffectBase GetImageEffectBase(this GameObject self) => self.GetComponent<UnityStandardAssets.ImageEffects.ImageEffectBase>();
	public static UnityStandardAssets.ImageEffects.ImageEffectBase GetImageEffectBase(this Component self) => self.GetComponent<UnityStandardAssets.ImageEffects.ImageEffectBase>();

	public static UnityStandardAssets.ImageEffects.MotionBlur GetMotionBlur(this GameObject self) => self.GetComponent<UnityStandardAssets.ImageEffects.MotionBlur>();
	public static UnityStandardAssets.ImageEffects.MotionBlur GetMotionBlur(this Component self) => self.GetComponent<UnityStandardAssets.ImageEffects.MotionBlur>();

	public static UnityStandardAssets.ImageEffects.NoiseAndGrain GetNoiseAndGrain(this GameObject self) => self.GetComponent<UnityStandardAssets.ImageEffects.NoiseAndGrain>();
	public static UnityStandardAssets.ImageEffects.NoiseAndGrain GetNoiseAndGrain(this Component self) => self.GetComponent<UnityStandardAssets.ImageEffects.NoiseAndGrain>();

	public static UnityStandardAssets.ImageEffects.NoiseAndScratches GetNoiseAndScratches(this GameObject self) => self.GetComponent<UnityStandardAssets.ImageEffects.NoiseAndScratches>();
	public static UnityStandardAssets.ImageEffects.NoiseAndScratches GetNoiseAndScratches(this Component self) => self.GetComponent<UnityStandardAssets.ImageEffects.NoiseAndScratches>();

	public static UnityStandardAssets.ImageEffects.PostEffectsBase GetPostEffectsBase(this GameObject self) => self.GetComponent<UnityStandardAssets.ImageEffects.PostEffectsBase>();
	public static UnityStandardAssets.ImageEffects.PostEffectsBase GetPostEffectsBase(this Component self) => self.GetComponent<UnityStandardAssets.ImageEffects.PostEffectsBase>();

	public static UnityStandardAssets.ImageEffects.ScreenOverlay GetScreenOverlay(this GameObject self) => self.GetComponent<UnityStandardAssets.ImageEffects.ScreenOverlay>();
	public static UnityStandardAssets.ImageEffects.ScreenOverlay GetScreenOverlay(this Component self) => self.GetComponent<UnityStandardAssets.ImageEffects.ScreenOverlay>();

	public static UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion GetScreenSpaceAmbientOcclusion(this GameObject self) => self.GetComponent<UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion>();
	public static UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion GetScreenSpaceAmbientOcclusion(this Component self) => self.GetComponent<UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion>();

	public static UnityStandardAssets.ImageEffects.SepiaTone GetSepiaTone(this GameObject self) => self.GetComponent<UnityStandardAssets.ImageEffects.SepiaTone>();
	public static UnityStandardAssets.ImageEffects.SepiaTone GetSepiaTone(this Component self) => self.GetComponent<UnityStandardAssets.ImageEffects.SepiaTone>();

	public static UnityStandardAssets.ImageEffects.SunShafts GetSunShafts(this GameObject self) => self.GetComponent<UnityStandardAssets.ImageEffects.SunShafts>();
	public static UnityStandardAssets.ImageEffects.SunShafts GetSunShafts(this Component self) => self.GetComponent<UnityStandardAssets.ImageEffects.SunShafts>();

	public static UnityStandardAssets.ImageEffects.Tonemapping GetTonemapping(this GameObject self) => self.GetComponent<UnityStandardAssets.ImageEffects.Tonemapping>();
	public static UnityStandardAssets.ImageEffects.Tonemapping GetTonemapping(this Component self) => self.GetComponent<UnityStandardAssets.ImageEffects.Tonemapping>();

	public static UnityStandardAssets.ImageEffects.Twirl GetTwirl(this GameObject self) => self.GetComponent<UnityStandardAssets.ImageEffects.Twirl>();
	public static UnityStandardAssets.ImageEffects.Twirl GetTwirl(this Component self) => self.GetComponent<UnityStandardAssets.ImageEffects.Twirl>();

	public static UnityStandardAssets.ImageEffects.VignetteAndChromaticAberration GetVignetteAndChromaticAberration(this GameObject self) => self.GetComponent<UnityStandardAssets.ImageEffects.VignetteAndChromaticAberration>();
	public static UnityStandardAssets.ImageEffects.VignetteAndChromaticAberration GetVignetteAndChromaticAberration(this Component self) => self.GetComponent<UnityStandardAssets.ImageEffects.VignetteAndChromaticAberration>();

	public static UnityStandardAssets.ImageEffects.Vortex GetVortex(this GameObject self) => self.GetComponent<UnityStandardAssets.ImageEffects.Vortex>();
	public static UnityStandardAssets.ImageEffects.Vortex GetVortex(this Component self) => self.GetComponent<UnityStandardAssets.ImageEffects.Vortex>();

	public static ToonyColors.TCF_Demo GetTCF_Demo(this GameObject self) => self.GetComponent<ToonyColors.TCF_Demo>();
	public static ToonyColors.TCF_Demo GetTCF_Demo(this Component self) => self.GetComponent<ToonyColors.TCF_Demo>();

	public static Assets.Scripts.BaseController GetBaseController(this GameObject self) => self.GetComponent<Assets.Scripts.BaseController>();
	public static Assets.Scripts.BaseController GetBaseController(this Component self) => self.GetComponent<Assets.Scripts.BaseController>();

	public static Assets.Scripts.BlastMagicController GetBlastMagicController(this GameObject self) => self.GetComponent<Assets.Scripts.BlastMagicController>();
	public static Assets.Scripts.BlastMagicController GetBlastMagicController(this Component self) => self.GetComponent<Assets.Scripts.BlastMagicController>();

	public static CameraController GetCameraController(this GameObject self) => self.GetComponent<CameraController>();
	public static CameraController GetCameraController(this Component self) => self.GetComponent<CameraController>();

	public static Assets.Scripts.DelegateScript GetDelegateScript(this GameObject self) => self.GetComponent<Assets.Scripts.DelegateScript>();
	public static Assets.Scripts.DelegateScript GetDelegateScript(this Component self) => self.GetComponent<Assets.Scripts.DelegateScript>();

	public static Assets.Scripts.Global GetGlobal(this GameObject self) => self.GetComponent<Assets.Scripts.Global>();
	public static Assets.Scripts.Global GetGlobal(this Component self) => self.GetComponent<Assets.Scripts.Global>();

	public static GlowComposite GetGlowComposite(this GameObject self) => self.GetComponent<GlowComposite>();
	public static GlowComposite GetGlowComposite(this Component self) => self.GetComponent<GlowComposite>();

	public static GlowController GetGlowController(this GameObject self) => self.GetComponent<GlowController>();
	public static GlowController GetGlowController(this Component self) => self.GetComponent<GlowController>();

	public static GlowObject GetGlowObject(this GameObject self) => self.GetComponent<GlowObject>();
	public static GlowObject GetGlowObject(this Component self) => self.GetComponent<GlowObject>();

	public static GlowObjectCmd GetGlowObjectCmd(this GameObject self) => self.GetComponent<GlowObjectCmd>();
	public static GlowObjectCmd GetGlowObjectCmd(this Component self) => self.GetComponent<GlowObjectCmd>();

	public static GlowPrePass GetGlowPrePass(this GameObject self) => self.GetComponent<GlowPrePass>();
	public static GlowPrePass GetGlowPrePass(this Component self) => self.GetComponent<GlowPrePass>();

	public static Assets.Scripts.HealthBar GetHealthBar(this GameObject self) => self.GetComponent<Assets.Scripts.HealthBar>();
	public static Assets.Scripts.HealthBar GetHealthBar(this Component self) => self.GetComponent<Assets.Scripts.HealthBar>();

	public static Interactive GetInteractive(this GameObject self) => self.GetComponent<Interactive>();
	public static Interactive GetInteractive(this Component self) => self.GetComponent<Interactive>();

	public static Liftable GetLiftable(this GameObject self) => self.GetComponent<Liftable>();
	public static Liftable GetLiftable(this Component self) => self.GetComponent<Liftable>();

	public static OpenableDoor GetOpenableDoor(this GameObject self) => self.GetComponent<OpenableDoor>();
	public static OpenableDoor GetOpenableDoor(this Component self) => self.GetComponent<OpenableDoor>();

	public static ReadableBook GetReadableBook(this GameObject self) => self.GetComponent<ReadableBook>();
	public static ReadableBook GetReadableBook(this Component self) => self.GetComponent<ReadableBook>();

	public static Switch GetSwitch(this GameObject self) => self.GetComponent<Switch>();
	public static Switch GetSwitch(this Component self) => self.GetComponent<Switch>();

	public static PlayerController GetPlayerController(this GameObject self) => self.GetComponent<PlayerController>();
	public static PlayerController GetPlayerController(this Component self) => self.GetComponent<PlayerController>();

	public static Spinner GetSpinner(this GameObject self) => self.GetComponent<Spinner>();
	public static Spinner GetSpinner(this Component self) => self.GetComponent<Spinner>();

	public static Assets.Scripts.TSController GetTSController(this GameObject self) => self.GetComponent<Assets.Scripts.TSController>();
	public static Assets.Scripts.TSController GetTSController(this Component self) => self.GetComponent<Assets.Scripts.TSController>();

	public static UnityEngine.EventSystems.EventSystem GetEventSystem(this GameObject self) => self.GetComponent<UnityEngine.EventSystems.EventSystem>();
	public static UnityEngine.EventSystems.EventSystem GetEventSystem(this Component self) => self.GetComponent<UnityEngine.EventSystems.EventSystem>();

	public static UnityEngine.EventSystems.EventTrigger GetEventTrigger(this GameObject self) => self.GetComponent<UnityEngine.EventSystems.EventTrigger>();
	public static UnityEngine.EventSystems.EventTrigger GetEventTrigger(this Component self) => self.GetComponent<UnityEngine.EventSystems.EventTrigger>();

	public static UnityEngine.EventSystems.UIBehaviour GetUIBehaviour(this GameObject self) => self.GetComponent<UnityEngine.EventSystems.UIBehaviour>();
	public static UnityEngine.EventSystems.UIBehaviour GetUIBehaviour(this Component self) => self.GetComponent<UnityEngine.EventSystems.UIBehaviour>();

	public static UnityEngine.EventSystems.BaseInput GetBaseInput(this GameObject self) => self.GetComponent<UnityEngine.EventSystems.BaseInput>();
	public static UnityEngine.EventSystems.BaseInput GetBaseInput(this Component self) => self.GetComponent<UnityEngine.EventSystems.BaseInput>();

	public static UnityEngine.EventSystems.BaseInputModule GetBaseInputModule(this GameObject self) => self.GetComponent<UnityEngine.EventSystems.BaseInputModule>();
	public static UnityEngine.EventSystems.BaseInputModule GetBaseInputModule(this Component self) => self.GetComponent<UnityEngine.EventSystems.BaseInputModule>();

	public static UnityEngine.EventSystems.PointerInputModule GetPointerInputModule(this GameObject self) => self.GetComponent<UnityEngine.EventSystems.PointerInputModule>();
	public static UnityEngine.EventSystems.PointerInputModule GetPointerInputModule(this Component self) => self.GetComponent<UnityEngine.EventSystems.PointerInputModule>();

	public static UnityEngine.EventSystems.StandaloneInputModule GetStandaloneInputModule(this GameObject self) => self.GetComponent<UnityEngine.EventSystems.StandaloneInputModule>();
	public static UnityEngine.EventSystems.StandaloneInputModule GetStandaloneInputModule(this Component self) => self.GetComponent<UnityEngine.EventSystems.StandaloneInputModule>();

	public static UnityEngine.EventSystems.BaseRaycaster GetBaseRaycaster(this GameObject self) => self.GetComponent<UnityEngine.EventSystems.BaseRaycaster>();
	public static UnityEngine.EventSystems.BaseRaycaster GetBaseRaycaster(this Component self) => self.GetComponent<UnityEngine.EventSystems.BaseRaycaster>();

	public static UnityEngine.EventSystems.Physics2DRaycaster GetPhysics2DRaycaster(this GameObject self) => self.GetComponent<UnityEngine.EventSystems.Physics2DRaycaster>();
	public static UnityEngine.EventSystems.Physics2DRaycaster GetPhysics2DRaycaster(this Component self) => self.GetComponent<UnityEngine.EventSystems.Physics2DRaycaster>();

	public static UnityEngine.EventSystems.PhysicsRaycaster GetPhysicsRaycaster(this GameObject self) => self.GetComponent<UnityEngine.EventSystems.PhysicsRaycaster>();
	public static UnityEngine.EventSystems.PhysicsRaycaster GetPhysicsRaycaster(this Component self) => self.GetComponent<UnityEngine.EventSystems.PhysicsRaycaster>();

	public static UnityEngine.UI.Button GetButton(this GameObject self) => self.GetComponent<UnityEngine.UI.Button>();
	public static UnityEngine.UI.Button GetButton(this Component self) => self.GetComponent<UnityEngine.UI.Button>();

	public static UnityEngine.UI.Dropdown GetDropdown(this GameObject self) => self.GetComponent<UnityEngine.UI.Dropdown>();
	public static UnityEngine.UI.Dropdown GetDropdown(this Component self) => self.GetComponent<UnityEngine.UI.Dropdown>();

	public static UnityEngine.UI.Graphic GetGraphic(this GameObject self) => self.GetComponent<UnityEngine.UI.Graphic>();
	public static UnityEngine.UI.Graphic GetGraphic(this Component self) => self.GetComponent<UnityEngine.UI.Graphic>();

	public static UnityEngine.UI.GraphicRaycaster GetGraphicRaycaster(this GameObject self) => self.GetComponent<UnityEngine.UI.GraphicRaycaster>();
	public static UnityEngine.UI.GraphicRaycaster GetGraphicRaycaster(this Component self) => self.GetComponent<UnityEngine.UI.GraphicRaycaster>();

	public static UnityEngine.UI.Image GetImage(this GameObject self) => self.GetComponent<UnityEngine.UI.Image>();
	public static UnityEngine.UI.Image GetImage(this Component self) => self.GetComponent<UnityEngine.UI.Image>();

	public static UnityEngine.UI.InputField GetInputField(this GameObject self) => self.GetComponent<UnityEngine.UI.InputField>();
	public static UnityEngine.UI.InputField GetInputField(this Component self) => self.GetComponent<UnityEngine.UI.InputField>();

	public static UnityEngine.UI.Mask GetMask(this GameObject self) => self.GetComponent<UnityEngine.UI.Mask>();
	public static UnityEngine.UI.Mask GetMask(this Component self) => self.GetComponent<UnityEngine.UI.Mask>();

	public static UnityEngine.UI.MaskableGraphic GetMaskableGraphic(this GameObject self) => self.GetComponent<UnityEngine.UI.MaskableGraphic>();
	public static UnityEngine.UI.MaskableGraphic GetMaskableGraphic(this Component self) => self.GetComponent<UnityEngine.UI.MaskableGraphic>();

	public static UnityEngine.UI.RawImage GetRawImage(this GameObject self) => self.GetComponent<UnityEngine.UI.RawImage>();
	public static UnityEngine.UI.RawImage GetRawImage(this Component self) => self.GetComponent<UnityEngine.UI.RawImage>();

	public static UnityEngine.UI.RectMask2D GetRectMask2D(this GameObject self) => self.GetComponent<UnityEngine.UI.RectMask2D>();
	public static UnityEngine.UI.RectMask2D GetRectMask2D(this Component self) => self.GetComponent<UnityEngine.UI.RectMask2D>();

	public static UnityEngine.UI.Scrollbar GetScrollbar(this GameObject self) => self.GetComponent<UnityEngine.UI.Scrollbar>();
	public static UnityEngine.UI.Scrollbar GetScrollbar(this Component self) => self.GetComponent<UnityEngine.UI.Scrollbar>();

	public static UnityEngine.UI.ScrollRect GetScrollRect(this GameObject self) => self.GetComponent<UnityEngine.UI.ScrollRect>();
	public static UnityEngine.UI.ScrollRect GetScrollRect(this Component self) => self.GetComponent<UnityEngine.UI.ScrollRect>();

	public static UnityEngine.UI.Selectable GetSelectable(this GameObject self) => self.GetComponent<UnityEngine.UI.Selectable>();
	public static UnityEngine.UI.Selectable GetSelectable(this Component self) => self.GetComponent<UnityEngine.UI.Selectable>();

	public static UnityEngine.UI.Slider GetSlider(this GameObject self) => self.GetComponent<UnityEngine.UI.Slider>();
	public static UnityEngine.UI.Slider GetSlider(this Component self) => self.GetComponent<UnityEngine.UI.Slider>();

	public static UnityEngine.UI.Text GetText(this GameObject self) => self.GetComponent<UnityEngine.UI.Text>();
	public static UnityEngine.UI.Text GetText(this Component self) => self.GetComponent<UnityEngine.UI.Text>();

	public static UnityEngine.UI.Toggle GetToggle(this GameObject self) => self.GetComponent<UnityEngine.UI.Toggle>();
	public static UnityEngine.UI.Toggle GetToggle(this Component self) => self.GetComponent<UnityEngine.UI.Toggle>();

	public static UnityEngine.UI.ToggleGroup GetToggleGroup(this GameObject self) => self.GetComponent<UnityEngine.UI.ToggleGroup>();
	public static UnityEngine.UI.ToggleGroup GetToggleGroup(this Component self) => self.GetComponent<UnityEngine.UI.ToggleGroup>();

	public static UnityEngine.UI.AspectRatioFitter GetAspectRatioFitter(this GameObject self) => self.GetComponent<UnityEngine.UI.AspectRatioFitter>();
	public static UnityEngine.UI.AspectRatioFitter GetAspectRatioFitter(this Component self) => self.GetComponent<UnityEngine.UI.AspectRatioFitter>();

	public static UnityEngine.UI.CanvasScaler GetCanvasScaler(this GameObject self) => self.GetComponent<UnityEngine.UI.CanvasScaler>();
	public static UnityEngine.UI.CanvasScaler GetCanvasScaler(this Component self) => self.GetComponent<UnityEngine.UI.CanvasScaler>();

	public static UnityEngine.UI.ContentSizeFitter GetContentSizeFitter(this GameObject self) => self.GetComponent<UnityEngine.UI.ContentSizeFitter>();
	public static UnityEngine.UI.ContentSizeFitter GetContentSizeFitter(this Component self) => self.GetComponent<UnityEngine.UI.ContentSizeFitter>();

	public static UnityEngine.UI.GridLayoutGroup GetGridLayoutGroup(this GameObject self) => self.GetComponent<UnityEngine.UI.GridLayoutGroup>();
	public static UnityEngine.UI.GridLayoutGroup GetGridLayoutGroup(this Component self) => self.GetComponent<UnityEngine.UI.GridLayoutGroup>();

	public static UnityEngine.UI.HorizontalLayoutGroup GetHorizontalLayoutGroup(this GameObject self) => self.GetComponent<UnityEngine.UI.HorizontalLayoutGroup>();
	public static UnityEngine.UI.HorizontalLayoutGroup GetHorizontalLayoutGroup(this Component self) => self.GetComponent<UnityEngine.UI.HorizontalLayoutGroup>();

	public static UnityEngine.UI.HorizontalOrVerticalLayoutGroup GetHorizontalOrVerticalLayoutGroup(this GameObject self) => self.GetComponent<UnityEngine.UI.HorizontalOrVerticalLayoutGroup>();
	public static UnityEngine.UI.HorizontalOrVerticalLayoutGroup GetHorizontalOrVerticalLayoutGroup(this Component self) => self.GetComponent<UnityEngine.UI.HorizontalOrVerticalLayoutGroup>();

	public static UnityEngine.UI.LayoutElement GetLayoutElement(this GameObject self) => self.GetComponent<UnityEngine.UI.LayoutElement>();
	public static UnityEngine.UI.LayoutElement GetLayoutElement(this Component self) => self.GetComponent<UnityEngine.UI.LayoutElement>();

	public static UnityEngine.UI.LayoutGroup GetLayoutGroup(this GameObject self) => self.GetComponent<UnityEngine.UI.LayoutGroup>();
	public static UnityEngine.UI.LayoutGroup GetLayoutGroup(this Component self) => self.GetComponent<UnityEngine.UI.LayoutGroup>();

	public static UnityEngine.UI.VerticalLayoutGroup GetVerticalLayoutGroup(this GameObject self) => self.GetComponent<UnityEngine.UI.VerticalLayoutGroup>();
	public static UnityEngine.UI.VerticalLayoutGroup GetVerticalLayoutGroup(this Component self) => self.GetComponent<UnityEngine.UI.VerticalLayoutGroup>();

	public static UnityEngine.UI.BaseMeshEffect GetBaseMeshEffect(this GameObject self) => self.GetComponent<UnityEngine.UI.BaseMeshEffect>();
	public static UnityEngine.UI.BaseMeshEffect GetBaseMeshEffect(this Component self) => self.GetComponent<UnityEngine.UI.BaseMeshEffect>();

	public static UnityEngine.UI.Outline GetOutline(this GameObject self) => self.GetComponent<UnityEngine.UI.Outline>();
	public static UnityEngine.UI.Outline GetOutline(this Component self) => self.GetComponent<UnityEngine.UI.Outline>();

	public static UnityEngine.UI.PositionAsUV1 GetPositionAsUV1(this GameObject self) => self.GetComponent<UnityEngine.UI.PositionAsUV1>();
	public static UnityEngine.UI.PositionAsUV1 GetPositionAsUV1(this Component self) => self.GetComponent<UnityEngine.UI.PositionAsUV1>();

	public static UnityEngine.UI.Shadow GetShadow(this GameObject self) => self.GetComponent<UnityEngine.UI.Shadow>();
	public static UnityEngine.UI.Shadow GetShadow(this Component self) => self.GetComponent<UnityEngine.UI.Shadow>();

	public static UnityEngine.Networking.NetworkAnimator GetNetworkAnimator(this GameObject self) => self.GetComponent<UnityEngine.Networking.NetworkAnimator>();
	public static UnityEngine.Networking.NetworkAnimator GetNetworkAnimator(this Component self) => self.GetComponent<UnityEngine.Networking.NetworkAnimator>();

	public static UnityEngine.Networking.NetworkBehaviour GetNetworkBehaviour(this GameObject self) => self.GetComponent<UnityEngine.Networking.NetworkBehaviour>();
	public static UnityEngine.Networking.NetworkBehaviour GetNetworkBehaviour(this Component self) => self.GetComponent<UnityEngine.Networking.NetworkBehaviour>();

	public static UnityEngine.Networking.NetworkDiscovery GetNetworkDiscovery(this GameObject self) => self.GetComponent<UnityEngine.Networking.NetworkDiscovery>();
	public static UnityEngine.Networking.NetworkDiscovery GetNetworkDiscovery(this Component self) => self.GetComponent<UnityEngine.Networking.NetworkDiscovery>();

	public static UnityEngine.Networking.NetworkIdentity GetNetworkIdentity(this GameObject self) => self.GetComponent<UnityEngine.Networking.NetworkIdentity>();
	public static UnityEngine.Networking.NetworkIdentity GetNetworkIdentity(this Component self) => self.GetComponent<UnityEngine.Networking.NetworkIdentity>();

	public static UnityEngine.Networking.NetworkLobbyManager GetNetworkLobbyManager(this GameObject self) => self.GetComponent<UnityEngine.Networking.NetworkLobbyManager>();
	public static UnityEngine.Networking.NetworkLobbyManager GetNetworkLobbyManager(this Component self) => self.GetComponent<UnityEngine.Networking.NetworkLobbyManager>();

	public static UnityEngine.Networking.NetworkLobbyPlayer GetNetworkLobbyPlayer(this GameObject self) => self.GetComponent<UnityEngine.Networking.NetworkLobbyPlayer>();
	public static UnityEngine.Networking.NetworkLobbyPlayer GetNetworkLobbyPlayer(this Component self) => self.GetComponent<UnityEngine.Networking.NetworkLobbyPlayer>();

	public static UnityEngine.Networking.NetworkManager GetNetworkManager(this GameObject self) => self.GetComponent<UnityEngine.Networking.NetworkManager>();
	public static UnityEngine.Networking.NetworkManager GetNetworkManager(this Component self) => self.GetComponent<UnityEngine.Networking.NetworkManager>();

	public static UnityEngine.Networking.NetworkManagerHUD GetNetworkManagerHUD(this GameObject self) => self.GetComponent<UnityEngine.Networking.NetworkManagerHUD>();
	public static UnityEngine.Networking.NetworkManagerHUD GetNetworkManagerHUD(this Component self) => self.GetComponent<UnityEngine.Networking.NetworkManagerHUD>();

	public static UnityEngine.Networking.NetworkMigrationManager GetNetworkMigrationManager(this GameObject self) => self.GetComponent<UnityEngine.Networking.NetworkMigrationManager>();
	public static UnityEngine.Networking.NetworkMigrationManager GetNetworkMigrationManager(this Component self) => self.GetComponent<UnityEngine.Networking.NetworkMigrationManager>();

	public static UnityEngine.Networking.NetworkProximityChecker GetNetworkProximityChecker(this GameObject self) => self.GetComponent<UnityEngine.Networking.NetworkProximityChecker>();
	public static UnityEngine.Networking.NetworkProximityChecker GetNetworkProximityChecker(this Component self) => self.GetComponent<UnityEngine.Networking.NetworkProximityChecker>();

	public static UnityEngine.Networking.NetworkStartPosition GetNetworkStartPosition(this GameObject self) => self.GetComponent<UnityEngine.Networking.NetworkStartPosition>();
	public static UnityEngine.Networking.NetworkStartPosition GetNetworkStartPosition(this Component self) => self.GetComponent<UnityEngine.Networking.NetworkStartPosition>();

	public static UnityEngine.Networking.NetworkTransformChild GetNetworkTransformChild(this GameObject self) => self.GetComponent<UnityEngine.Networking.NetworkTransformChild>();
	public static UnityEngine.Networking.NetworkTransformChild GetNetworkTransformChild(this Component self) => self.GetComponent<UnityEngine.Networking.NetworkTransformChild>();

	public static UnityEngine.Networking.NetworkTransform GetNetworkTransform(this GameObject self) => self.GetComponent<UnityEngine.Networking.NetworkTransform>();
	public static UnityEngine.Networking.NetworkTransform GetNetworkTransform(this Component self) => self.GetComponent<UnityEngine.Networking.NetworkTransform>();

	public static UnityEngine.Networking.NetworkTransformVisualizer GetNetworkTransformVisualizer(this GameObject self) => self.GetComponent<UnityEngine.Networking.NetworkTransformVisualizer>();
	public static UnityEngine.Networking.NetworkTransformVisualizer GetNetworkTransformVisualizer(this Component self) => self.GetComponent<UnityEngine.Networking.NetworkTransformVisualizer>();
}