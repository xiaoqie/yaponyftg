﻿using System;
using UnityEngine;

namespace Assets.Scripts
{
	public class HealthBar : MonoBehaviour
	{
		private RectTransform tf;

		[NonSerialized]
		public float max = 100;

		// Use this for initialization
		void Start ()
		{
			tf = GetComponent<RectTransform>();
		}

		public void Reduce(float hp)
		{
			tf.localScale = new Vector3(tf.localScale.x - 5*hp/max, tf.localScale.y, tf.localScale.z);
		}
	}
}
