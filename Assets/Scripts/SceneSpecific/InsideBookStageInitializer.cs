﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InsideBookStageInitializer : MonoBehaviour {

    public GameObject mainCamera;
    public GameObject player;

    public int checkpoint2load_debug = 0;

    private void Awake() {
        if (SceneParameters.Checkpoint2Load == null)
            SceneParameters.Checkpoint2Load = checkpoint2load_debug;
    }

    // Use this for initialization
    void Start() {

        Transform spawner;
        spawner = transform.Find($"PlayerSpawner#{SceneParameters.Checkpoint2Load}");
        if (spawner == null) {
            spawner = transform.Find("PlayerSpawner#-1");
            print("the number is invalid");
        }
        //Instantiate(mainCamera);
        var ts = Instantiate(player);
        ts.transform.position = spawner.position;
        //ts.GetPlayerController().cameraMode = 0;
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}

/*
public class InsideBookStageInitializer : MonoBehaviour {

    public GameObject mainCamera;
    public GameObject player;

    public int checkpoint2load_debug = 0;

    private void Awake() {
        //Destroy(PlayerController.that.gameObject);
    }

    // Use this for initialization
    void Start() {

        Transform spawner;
        if (checkpoint2load_debug == 0) {
            if (SceneParameters.Checkpoint2Load == null) {
                spawner = transform.Find("PlayerSpawner");
            } else {
                spawner = transform.Find($"PlayerSpawners/PlayerSpawner${SceneParameters.Checkpoint2Load}");
            }
        } else {
            spawner = transform.Find($"PlayerSpawner#{checkpoint2load_debug}");
            SceneParameters.Checkpoint2Load = checkpoint2load_debug;
        }
        if (spawner == null) {
            spawner = transform.Find("PlayerSpawner");
            print("the number is invalid");
        }
        //Instantiate(mainCamera);
        var ts = Instantiate(player);
        ts.transform.position = spawner.position;
        //ts.GetPlayerController().cameraMode = 0;
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
*/