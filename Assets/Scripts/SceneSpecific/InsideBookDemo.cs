﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using static U;

public class InsideBookDemo : MonoBehaviour {
	// Use this for initialization
	void Start () {

        Action<int> controlVisibleBlock = (id) => {
            var ControlVisible = GameObject.Find($"ControlVisible#{id}"); // because set in script executation order, this will be executed before RendererFade set it to inactive.
            var fade = ControlVisible.GetComponent<Fade>();
            GameObject.Find($"Switch#{id}").gameObject.GetComponent<SwitchEvent>().SetPress(() => {
                fade?.FadeIn();
            }).SetRelease(() => {
                fade?.FadeOut();
            });
        };

        controlVisibleBlock(2);
        controlVisibleBlock(3);
        controlVisibleBlock(4);
        controlVisibleBlock(5);

        {
            var plat = GameObject.Find("SwitchLiftPlatform#6").GetComponent<SwitchLiftPlatform>();
            GameObject.Find("Switch#6").gameObject.GetComponent<SwitchEvent>().SetPress(() => {
                plat.up = true;
            }).SetRelease(() => {
                plat.up = false;
            });
        }

        {
            var sph = GameObject.Find("FireSphere#7").GetComponent<StartVelocity>();
            var initPos = sph.transform.position;
            var initVel = sph.velocity;
            GameObject.Find("Switch#7").gameObject.GetComponent<SwitchEvent>().SetPress(() => {
                //plat.up = true;
                sph.transform.position = initPos;
                sph.GetRigidbody().velocity = initVel;
            });
        }

        {
            var Text2 = GameObject.Find("Text2");
            var Cage3 = GameObject.Find("Cage#3");
            //Cage3.GetComponentInChildren<MeshRenderer>().material.SetFloat("_Mode", 2.0f);
            //Text2.gameObject.SetActive(false);
            GameObject.Find("Switch#1").gameObject.GetComponent<SwitchEvent>().OnPress = () => {
                Text2.gameObject.SetActive(true);
                Cage3.GetComponent<Fade>().FadeOut();
            };
        }

        {
            var Cage4 = GameObject.Find("Cage#4");
            GameObject.Find("TextReorderGroup").gameObject.GetComponent<TextReorderGroup>().OnAction = () => {
                Cage4.GetComponent<Fade>().FadeOut();
            };
        }

        Cursor.visible = false;
    }

    private void Update() {
        /*Camera.main.transform.aposition().z = -16; // We need a new Camera Controller
        Camera.main.GetComponent<CameraController>().offset = V(0,5,0);
        //Camera.main.GetComponent<CameraController>().followAxis = V(0, 1, 0);
        Camera.main.GetComponent<CameraController>().lookAt = false;*/
        if (SceneParameters.Checkpoint2Load == 8) {
            Camera.main.transform.aposition().z = -16;
            Camera.main.GetCameraController().BoundsStrategy = (bounds) => {
                bounds.max = V(bounds.min.x * 0.4f, bounds.min.y * 0.3f);
                bounds.min = V(bounds.min.x * 0.7f, bounds.min.y * 0.65f);
                return bounds;
            };
            Camera.main.GetComponent<VolumetricFog>().m_GlobalDensityMult = 0.5f;
        }

        //PlayerController.that.cameraMode = "Forward";
    }
}
