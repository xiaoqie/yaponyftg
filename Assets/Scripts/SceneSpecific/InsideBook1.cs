﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using static U;
using Assets.Scripts;
using UnityStandardAssets.ImageEffects;
using static UnityEngine.Mathf;

public class InsideBook1 : MonoBehaviour {
    // Use this for initialization
    void Start() {
        Action<int> controlVisibleBlock = (id) => {
            var ControlVisible = GameObject.Find($"ControlVisible#{id}"); // because set in script executation order, this will be executed before RendererFade set it to inactive.
            var fade = ControlVisible.GetComponent<Fade>();
            if (fade.Default) {
                GameObject.Find($"Switch#{id}").gameObject.GetComponent<SwitchEvent>().SetPress(() => {
                    fade?.FadeOut();
                }).SetRelease(() => {
                    fade?.FadeIn();
                });
            } else {
                GameObject.Find($"Switch#{id}").gameObject.GetComponent<SwitchEvent>().SetPress(() => {
                    fade?.FadeIn();
                }).SetRelease(() => {
                    fade?.FadeOut();
                });
            }
        };
        for (int i = 1; i <= 5; i++) {
            controlVisibleBlock(i);
        }

        Cursor.visible = false;
        // TODO: I found a bug, where the gaussion blur of the safe/death area is dependent on the screen size.
        // TODO: a bug when you gently touch a button, the button won't turn off.

        GameObject.Find("Trigger#1").AddComponent<DelegateScript>().M_OnTriggerEnter = (self, other) => {
            if (!other.CompareTag("Player")) return;

            var player = PlayerController.that;
            var camera = Camera.main;

            player.cameraMode = "None";
            player.SetCameraMode(); // this is bad interface, really bad.
            player.ForceHorizontalInput(0);
            StartCoroutine(WaitForSecondsCoroutine(1, () => {
                player.ForceHorizontalInput(1);
                StartCoroutine(WaitForSecondsCoroutine(5, () => {
                    player.ForceHorizontalInput(null);

                    player.movable = false;
                    float t = 1;
                    camera.GetCameraController().cameraMode = CameraController.CameraMode.None;
                    StartCoroutine(WaitForSecondsCoroutine(1, () => {
                        var pp = transform.Find("PaperPP");
                        StartCoroutine(LerpCoroutine(v => camera.transform.position = v, camera.transform.position, player.transform.position + V(0, 3, -8), t));
                        StartCoroutine(LerpCoroutine(v => {
                            camera.transform.eulerAngles = v;
                            // pp.right = (camera.transform.position - pp.position).normalized; how do we deal with pp's orientation?
                        }, camera.transform.eulerAngles, V(0, 0, 0), t,
                            final: () => {
                                var death = transform.Find("DeathArea#1");
                                death.gameObject.SetActive(true);
                                var oldScale = death.localScale;
                                death.localScale = V0;
                                StartCoroutine(LerpCoroutine(v => death.localScale = v, oldScale * 0.6f, oldScale, 3, final: () => {
                                    camera.GetCameraController().cameraMode = CameraController.CameraMode.Mario;
                                    player.movable = true;

                                    player.cameraMode = "Default";
                                    player.SetCameraMode(); // this is bad interface, really bad.

                            DepthOfField dof = camera.GetComponent<DepthOfField>();
                                    StartCoroutine(LerpCoroutine(v => dof.aperture = v, dof.aperture, 0, 1, final: () => dof.enabled = false));
                                }));
                            }));
                    }));
                }));
            }));
            
            
            self.M_OnTriggerEnter = null;
        };

        GameObject.Find("Trigger#2").AddComponent<DelegateScript>().M_OnTriggerEnter = (self, other) => {
            if (!other.CompareTag("Player")) return;

            var player = PlayerController.that;
            var camera = Camera.main;

            var rd = GameObject.Find("PaperRD").GetComponent<Animator>();

            player.movable = false;

            rd.Play("Kick");
            StartCoroutine(WaitForSecondsCoroutine(0.4f, () => GameObject.Find("smashable (1)").GetComponent<Smashable>().Smash()));

            StartCoroutine(WaitForSecondsCoroutine(0.5f, () => {
                player.movable = true;
                StartCoroutine(LerpCoroutine(v => rd.transform.position = v, rd.transform.position, rd.transform.position + V(5, 1, 0), 1, final: () => {
                    rd.Play("Kick");
                    StartCoroutine(WaitForSecondsCoroutine(0.4f, () => GameObject.Find("smashable (2)").GetComponent<Smashable>().Smash()));
                    StartCoroutine(LerpCoroutine(v => rd.transform.position = v, rd.transform.position, GameObject.Find("smashable (3)").transform.position + V(-5, 3, 0), 3, final: () => { rd.Play("Kick"); }));
                }));
            }));
        };

        GameObject.Find("Trigger#3").AddComponent<DelegateScript>().M_OnTriggerEnter = (self, other) => {
            if (!other.CompareTag("Player")) return;

            var player = PlayerController.that;
            var camera = Camera.main;

            var rd = GameObject.Find("PaperRD").GetComponent<Animator>();

            float t = 0.8f;
            StartCoroutine(LerpCoroutine(v => rd.transform.position = v, rd.transform.position, GameObject.Find("smashable (3)").transform.position - V(2, 0, 0), 1 - 0.4f, final: () => { rd.Play("Kick"); }));
            StartCoroutine(WaitForSecondsCoroutine(1f, () => {
                GameObject.Find("smashable (3)").GetComponent<Smashable>().Smash();
                StartCoroutine(LerpCoroutine(v => rd.transform.position = v, rd.transform.position, GameObject.Find("smashable (4)").transform.position - V(2, 0, 0), t - 0.4f, final: () => { rd.Play("Kick"); }));
                StartCoroutine(WaitForSecondsCoroutine(t, () => {
                    GameObject.Find("smashable (4)").GetComponent<Smashable>().Smash();
                    StartCoroutine(LerpCoroutine(v => rd.transform.position = v, rd.transform.position, GameObject.Find("smashable (5)").transform.position - V(2, 0, 0), t + 0.2f - 0.4f, final: () => { rd.Play("Kick"); }));
                    StartCoroutine(WaitForSecondsCoroutine(t + 0.2f, () => {
                        GameObject.Find("smashable (5)").GetComponent<Smashable>().Smash();
                        StartCoroutine(LerpCoroutine(v => rd.transform.position = v, rd.transform.position, GameObject.Find("smashable (6)").transform.position - V(2, 0, 0), t - 0.4f, final: () => { rd.Play("Kick"); }));
                        StartCoroutine(WaitForSecondsCoroutine(t, () => {
                            GameObject.Find("smashable (6)").GetComponent<Smashable>().Smash();
                            StartCoroutine(LerpCoroutine(v => rd.transform.position = v, rd.transform.position, GameObject.Find("smashable (7)").transform.position - V(2, 0, 0), t - 0.4f, final: () => { rd.Play("Kick"); }));
                            StartCoroutine(WaitForSecondsCoroutine(t, () => {
                                GameObject.Find("smashable (7)").GetComponent<Smashable>().Smash(); // lemme count... eight indents! yay~ linus! but still this is a just-happen-to-work solution.
                            }));
                        }));
                    }));
                }));
            }));
        };

        Camera.main.GetCameraController().BoundsStrategy = (bounds) => {
            bounds.max = V(bounds.min.x * 0, 0);
            bounds.min = V(bounds.min.x * 0, bounds.min.y * 0.7f);
            return bounds;
        };
        {
            var player = PlayerController.that;
            var camera = Camera.main;
            switch (SceneParameters.Checkpoint2Load) {
                case -1: // debug case
                case 4:
                case 3: {
                    var death = transform.Find("DeathArea#1");
                    death.gameObject.SetActive(true);

                    transform.Find("Lamp (4)").GetChild(0).aposition().x = player.transform.position.x;
                    //transform.Find("Lamp (4)").position += V(50 + 20 + 30, 0, 0);
                    goto case 2;
                }
                case 2: {
                    GameObject.Find("Trigger#1").GetComponent<DelegateScript>().M_OnTriggerEnter = null;

                    camera.transform.position = player.transform.position + V(0, 2, -8);
                    camera.GetCameraController().cameraMode = CameraController.CameraMode.Mario;
                    player.cameraMode = "Default";
                    player.SetCameraMode(); // this is bad interface, really bad.

                    var death = transform.Find("DeathArea#1");
                    death.gameObject.SetActive(true);

                    camera.GetComponent<DepthOfField>().enabled = false;
                    break;
                }
                case 1: {
                    player.cameraMode = "Forward";
                    player.SetCameraMode(); // this is bad interface, really bad.
                    break;
                }
            }
        }

        { // one may wonder, if this kind of block should be placed in a seperate function, and name it.
            StartCoroutine(WaitForSecondsCoroutine(1, () => {
                var head0 = GameObject.Find("PaperFS").transform.Find("Head0").GetSpriteRenderer();
                var head1 = GameObject.Find("PaperFS").transform.Find("Head1").GetSpriteRenderer();
                float t = 1;
                StartCoroutine(LerpCoroutine(v => { var c = head0.color; c.a = v; head0.color = c; }, 1, 0, t));
                StartCoroutine(LerpCoroutine(v => { var c = head1.color; c.a = v; head1.color = c; }, 0, 1, t));

                //Camera.main.GetGrayscale().enabled = true;
            }));
        }
    }

    private void Update() {
        /*
        if (SceneParameters.Checkpoint2Load == 8) {
            Camera.main.transform.aposition().z = -16;
            Camera.main.GetCameraController().BoundsStrategy = (bounds) => {
                bounds.max = V(bounds.min.x * 0.4f, bounds.min.y * 0.3f);
                bounds.min = V(bounds.min.x * 0.7f, bounds.min.y * 0.65f);
                return bounds;
            };
            Camera.main.GetComponent<VolumetricFog>().m_GlobalDensityMult = 0.5f;
        }*/
    }
}
