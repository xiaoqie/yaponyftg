﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.Mathf;
using static U;

public class SwitchLiftPlatform : MonoBehaviour {
    private Vector3 initPos;
    private Rigidbody rb;
    public float endIncrementalY;

    public bool up;
	// Use this for initialization
	void Start () {
        initPos = transform.position;
        rb = this.GetRigidbody();
	}
	
	// Update is called once per frame
	void Update () {
        transform.aposition().y = Clamp(transform.position.y, Min(initPos.y, initPos.y + endIncrementalY), Max(initPos.y, initPos.y + endIncrementalY)); // endIncrementalY may never be smaller than 0, but just in case.
        if (up && transform.position.y < initPos.y + endIncrementalY - 0.1f) {
            rb.velocity = V(0, 5, 0);
        } else if (!up && transform.position.y > initPos.y + 0.1f) {
            rb.velocity = V(0, -5, 0);
        } else {
            rb.velocity = V(0, 0, 0);
        }
	}
}
