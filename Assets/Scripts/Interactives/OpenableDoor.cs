﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenableDoor : Interactive {

	// Use this for initialization
	void Start () {
		
	}
    private bool open = false;
    private float openTime;
    private bool close = false;
    private float closeTime;
    // Update is called once per frame
    void Update () {
		if (open) {
            transform.GetChild(0).localEulerAngles = new Vector3(Mathf.Lerp(0, 89, Time.time - openTime), 0, 0);
            transform.GetChild(1).localEulerAngles = -new Vector3(Mathf.Lerp(0, 89, Time.time - openTime), 0, 0);
        }
        if (close) {
            transform.GetChild(0).localEulerAngles = new Vector3(Mathf.Lerp(89, 0, Time.time - closeTime), 0, 0);
            transform.GetChild(1).localEulerAngles = -new Vector3(Mathf.Lerp(89, 0, Time.time - closeTime), 0, 0);
        }
    }

    public override void Do() {
        open = true;
        close = false;
        openTime = Time.time;
    }

    public void Close() {
        close = true;
        open = false;
        closeTime = Time.time;
    }
}
