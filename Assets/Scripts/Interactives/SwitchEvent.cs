﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class SwitchEvent : MonoBehaviour {
    /*public virtual void OnPress() {
        // This is Temporary
    }
    public virtual void OnRelease() {

    }*/
    public Action OnPress = () => { };
    public Action OnRelease = () => { };

    public SwitchEvent SetPress(Action a) {
        OnPress = a;
        return this;
    }
    public SwitchEvent SetRelease(Action a) {
        OnRelease = a;
        return this;
    }
}
