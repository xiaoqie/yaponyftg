﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using static U;
using static UnityEngine.Mathf;
using V = UnityEngine.Vector3;

public class MouseParticle : MonoBehaviour {
    private void Update() {
        V mouse = Input.mousePosition;
        mouse = V(mouse.x / Screen.width, mouse.y / Screen.height);
        V world = Camera.main.ViewportToWorldPoint(V(mouse.x, mouse.y, 7));
        transform.position = world;
    }
}
