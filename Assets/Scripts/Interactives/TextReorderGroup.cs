﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

public class TextReorderGroup : MonoBehaviour {
    public List<Transform> objects;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        float min = -999999999;
		foreach (var o in objects) {
            if (o.position.x > min) {
                min = o.position.x;
            } else {
                return;
            }
        }
        if ((from o in objects select o.position.y).StdDev() > 0.2f) { // 0.2f, the magical number, and no one guarantees this works.
            return;
        }
        OnAction();
        //print("they are ordered now");
        enabled = false;
	}

    public Action OnAction = () => { };
}
