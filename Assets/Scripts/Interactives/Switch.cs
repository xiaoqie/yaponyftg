﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.Mathf;
using static U;
using V = UnityEngine.Vector3;

public class Switch : Liftable {
	
	private float initY;
	private Rigidbody rb;
	private bool isPushing;

    public bool on { get; private set; }
    private bool lastOn;
    private SwitchEvent evt;
	protected override void Start () {
		base.Start();
		initY = transform.position.y;
		rb = this.GetRigidbody();
        evt = GetComponent<SwitchEvent>();
	}

	private void Update() {
		if (transform.position.y > initY) {
            transform.aposition().y = initY;
			rb.velocity = V.zero;
        }
        lastOn = on;
        on = transform.position.y < initY - 0.1f;
        if (on & !lastOn) {
            evt?.OnPress();
        }
        if (on) {
            //print("chuu~");
        }
        if (!on & lastOn) {
            evt?.OnRelease();
        }
    }

	private void FixedUpdate() {
        if (transform.position.y < initY) 
            rb.AddForce(V(0, 1, 0));
		if (isPushing) {
			rb.AddForce(V(0, -5, 0));
			StartLifting();
			isPushing = false;
		} else {
			StopLifting();
		}
	}

	public void PushDown() {
		isPushing = true;
	}
}
