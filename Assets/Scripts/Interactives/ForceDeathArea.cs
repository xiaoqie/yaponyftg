﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForceDeathArea : MonoBehaviour {
    private void OnTriggerStay(Collider other) {
        if (other.CompareTag("Player")) {
            other.GetPlayerController().Die();
            Camera.main.GetCameraController().enabled = false;
        }
    }
}
