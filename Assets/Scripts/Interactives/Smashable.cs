﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Smashable : MonoBehaviour {
    public bool makeUninteractive = false;
    public float minSmashImpulse = 10;

    private void OnCollisionEnter(Collision collision) {
        if (collision.impulse.y > minSmashImpulse) { // magical theresold
            Smash();
        }
    }

    public void Smash() {
        this.GetBoxCollider().enabled = false;
        foreach (var collider in GetComponentsInChildren<MeshCollider>(true)) {
            collider.enabled = true;
            var originalPosition = collider.transform.position;
            GameObject hinge = new GameObject();
            hinge.transform.position = originalPosition;
            hinge.layer = collider.gameObject.layer = LayerMask.NameToLayer(makeUninteractive ? "Uninteractive" : "Liftable");
            hinge.transform.SetParent(collider.transform.parent);
            collider.transform.SetParent(hinge.transform);
            collider.gameObject.AddComponent<Liftable>();
            collider.transform.position = originalPosition;

            var rb = hinge.gameObject.AddComponent<Rigidbody>();
            //rb.AddExplosionForce(100, Vector3.one, 5f);
            rb.AddForce(500*(rb.transform.position - transform.position));
            rb.freezeRotation = true;

        }
    }
}
