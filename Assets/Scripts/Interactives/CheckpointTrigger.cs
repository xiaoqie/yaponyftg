﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using static U;
using static UnityEngine.Mathf;
using V = UnityEngine.Vector3;

public class CheckpointTrigger : MonoBehaviour {
    private int id; // the id is strictly increasing for now. when in checkpoint 2, checkpoint 1 is uninteractable.
    private void Start() {
        id = int.Parse(transform.parent.name.Split('#')[1]);
    }

    private void OnTriggerEnter(Collider other) {
        if (other.CompareTag("Player")) {
            if (id > SceneParameters.Checkpoint2Load) {
                print($"Checkpoint! No. {id}");
                SceneParameters.Checkpoint2Load = id;
            }
        }
    }
}
