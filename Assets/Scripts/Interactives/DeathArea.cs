﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathArea : MonoBehaviour {
    private void OnTriggerStay(Collider other) {
        if (other.CompareTag("Player")) {
            if (!other.GetPlayerController().safe) {
                //print("you are dead now");
                other.GetPlayerController().Die();
                Camera.main.GetCameraController().enabled = false;
            }
        }
    }
}
