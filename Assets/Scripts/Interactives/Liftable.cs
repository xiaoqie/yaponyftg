﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Liftable : MonoBehaviour {
	GlowObject glow;
	Color magicColor;
	float shiningCoefficient = 0.5f; // how much should this glow before lift
    public bool liftable = true; // false if player is on this, in order to obey newton's law.
	// Use this for initialization
	protected virtual void Start () {
		glow = GetComponent<GlowObject>();
		magicColor = glow.GlowColor;

		glow.GlowColor = magicColor * shiningCoefficient;
		glow.Glow();
	}

	public virtual void StartLifting() {
		glow.GlowColor = magicColor;
		glow.Glow();
	}

	public virtual void StopLifting() {
		glow.GlowColor = magicColor * shiningCoefficient;
		glow.Glow();
	}

    public virtual void TurnRed() {
        glow.GlowColor = Color.red;
        glow.Glow();
    }

    public MeshRenderer QindingMeshRenderer;

    public MeshRenderer GetQindingMeshRenderer() {
        if (QindingMeshRenderer)
            return QindingMeshRenderer;
        return this.GetMeshRenderer();
    }
}
