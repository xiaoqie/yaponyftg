﻿using System;
using System.Collections;
using Assets.Scripts.Automaton;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
	public class TSController : BaseController
	{
		public GameObject MagicBall;
		public GameObject BlastMagicBall;
		public GameObject HadouBall;

		private bool _magicBallDashing;
		private Vector3 _magicBallDestination;
		private Vector3 _magicBallVelocity;

		// Use this for initialization
		protected override void Start()
		{ // TODO: AIR ATTACK MAGIC ATTACK, DODGE PARTICLE EFFECT, INPUT BUFFERING, GROUND CHECK WHEN  LAUNCHING
		// // TODO: MAGIC BALL CONTROL OTHERS, Riding ,MAGIC BLAST CHARGE, MAGIC TELEPORTER, MAGIC GENERATE OBJECTS, MAGIC GESTURES, MAGIC BAR, HEALTH BAR
			base.Start();

			state = State.ReadFromConfigFile(Resources.Load<TextAsset>("Config/Twilight/input").text, this);
			if (state == null)
				Global.ShowDebugMessageOnScreen("XML Files Are Not Correct");

			MagicBall = (GameObject) Instantiate(MagicBall, transform.position + new Vector3(1, 1, 0), Quaternion.identity);
			MagicBall.GetComponent<DelegateScript>().M_OnTriggerEnter = (self, other) =>
			{
				if (other.CompareTag("PlayerHead") || other.CompareTag("PlayerBody"))
				{
					self.GetComponent<Collider>().enabled = false; // TODO !!! Problem: hit me but damage opponent
					Global.DealDamageTo(null, opponent.gameObject,
						new Damage
						{
							damage = 5,
							horizontalForce = 5,
							forceHorizontalDirection = new Vector3(self.GetComponent<Rigidbody>().velocity.x.Sign(), 0, 0)
						});
				}
			};
			Global.SetLayer(MagicBall, LayerMask.NameToLayer("Effect"));

			FindChild("BeamAttack").GetComponent<DelegateScript>().M_OnParticleCollision = (self, other) =>
			{
				if (other.CompareTag("Player")) // TODO !!! Problem: collider is player, can't decide head or body
				{
					Global.DealDamageTo(null, opponent.gameObject,
						new Damage
						{
							damage = 0.1f,
							horizontalForce = 1,
							forceHorizontalDirection = new Vector3((int) orientation, 0, 0),
							screenFrozenTime = 0.01f,
						});
				}
			};

			_magicBallDestination = MagicBall.transform.position;
		}

		protected override void Update()
		{// TODO PHYSICS PART IN FIXED UPDATE
			base.Update();

			MagicBall.GetComponent<Rigidbody>().AddForce(-1f * MagicBall.GetComponent<Rigidbody>().velocity);

			if (_magicBallDashing)
				MagicBall.transform.position = Vector3.SmoothDamp(MagicBall.transform.position, _magicBallDestination,
					ref _magicBallVelocity, 0.2f);
		}

		public void OnDestroy()
		{
			//Destroy(MagicBall);
		}

		// actions
		public void A()
		{
			animator.Play("a", 0);
			nextDamage = new Damage {damage = 3, horizontalForce = 5};
		}
		public void TapRightA()
		{
			animator.Play("tapright_a", 0);
			nextDamage = new Damage {damage = 3, horizontalForce = 5};
		}
		public void RightA()
		{
			animator.Play("right_a", 0);
			nextDamage = new Damage {damage = 3, horizontalForce = 15, type = Damage.Type.KNOCK_BACK};
		}

		public void StartLevitating()
		{
			rb.useGravity = false;
			transform.Find("MagicParticleEffect").GetComponent<ParticleSystem>().Play();
		}
		public void StopLevitating()
		{
			rb.useGravity = true;
			transform.Find("MagicParticleEffect").GetComponent<ParticleSystem>().Stop();
		}
		public void Levitate()
		{
			if (Mathf.Abs(Input.GetAxis(Control.AxisX)) > 0.1)
			{
				orientation = Input.GetAxis(Control.AxisX) < 0 ? Orientation.Left : Orientation.Right;
			}
			rb.AddForce(-5 * Input.GetAxis(Control.AxisX), 5 * Input.GetAxis(Control.AxisY), 0);
		}

		public void StartLevitatingMagicBall()
		{
			FindChild("HornEffect").GetComponent<ParticleSystem>().Play();
		}
		public void StopLevitatingMagicBall()
		{
			FindChild("HornEffect").GetComponent<ParticleSystem>().Stop();
		}
		public void LevitateMagicBall()
		{
			MagicBall.GetComponent<Rigidbody>().AddForce(-5 * Input.GetAxis(Control.AxisX), 5 * Input.GetAxis(Control.AxisY), 0);
		}
		public void PlaceBlastMagicBall()
		{
			Instantiate(BlastMagicBall, MagicBall.transform.position, Quaternion.identity);
		}

		public void StartMagicBallDashing()
		{
			MagicBall.GetComponent<Collider>().enabled = true;
			_magicBallDestination = MagicBall.transform.position + 2*GetInputDirection();
		}

		public void MagicBallDash()
		{
			_magicBallDashing = true;
		}

		public void StopMagicBallDashing()
		{
			_magicBallDashing = false;
			MagicBall.GetComponent<Collider>().enabled = false;
		}

		public void BeamAttack()
		{
			animator.SetFloat("beamattack_y", Input.GetAxis(Control.AxisY) == 0 ? 0 : Input.GetAxis(Control.AxisY));
			animator.SetFloat("beamattack_x", Input.GetAxis(Control.AxisX) > 0 ? Input.GetAxis(Control.AxisX) * 1.5f : 0);
		}

		public void StartBeamAttack()
		{
			FindChild("BeamAttack").GetComponent<ParticleSystem>().Play();
			animator.CrossFade("beam_attack", 0);
		}

		public void StopBeamAttack()
		{
			FindChild("BeamAttack").GetComponent<ParticleSystem>().Stop();
			animator.CrossFade("onGround", 0);
		}

		public void Dodge()
		{
			if (GetInputDirection().Equals(Vector3.zero))
			{
				transform.position = MagicBall.transform.position;
			}
			else
			{
				transform.position += 2*GetInputDirection();
			}
		}

		public void LayDownAttack()
		{
			animator.Play("lay_down_attack", 0);
			nextDamage = new Damage {damage = 3, type = Damage.Type.KNOCK_DOWN};
		}

		private static IEnumerator StartUpHadouBall_DestroyAfter(GameObject self)
		{
			yield return new WaitForSeconds(0.5f);
			self.GetComponent<ParticleSystem>().Stop();
			self.GetComponent<Collider>().enabled = false;
			yield return new WaitForSeconds(1);
			Destroy(self);
		}

		public void StartUpHadouBall()
		{
			GameObject ball = (GameObject) Instantiate(HadouBall, transform.position + (int) orientation*1.5f*Vector3.right - Vector3.up, Quaternion.identity);
			Global.SetLayer(ball, LayerMask.NameToLayer("Effect"));
			DelegateScript script = ball.GetComponent<DelegateScript>();
			script.M_Start = self =>
			{
				self.transform.eulerAngles = new Vector3(0, 0, -90);
				StartCoroutine(StartUpHadouBall_DestroyAfter(self.gameObject));
			};

			Vector3 dest = ball.transform.position + 4*Vector3.up;
			Vector3 vel = Vector3.zero;
			script.M_Update = self =>
			{
				self.transform.position = Vector3.SmoothDamp(self.transform.position, dest, ref vel, 0.5f);
			};

			script.M_OnTriggerEnter = (self, other) =>
			{
				if (other.CompareTag("PlayerHead") || other.CompareTag("PlayerBody"))
				{
					self.GetComponent<Collider>().enabled = false;
					Global.DealDamageTo(null, other.gameObject,
						new Damage
						{
							damage = 5,
							type = Damage.Type.LAUNCHER
						});
				}
			};
		}

		public void StopUpHadouBall()
		{
			
		}
	}
}
