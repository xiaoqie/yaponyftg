﻿using Assets.Scripts.Automaton;

namespace Assets.Scripts
{
	public class InputManager
	{
		State state;

		public InputManager(string xml)
		{
			state = State.ReadFromConfigFile(xml, this);
		}
	}
}