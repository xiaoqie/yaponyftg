﻿using System.Collections;
using UnityEngine;

namespace Assets.Scripts
{
	public class BlastMagicController : MonoBehaviour {

		// Use this for initialization
		void Start () {
			Global.SetLayer(gameObject, LayerMask.NameToLayer("Effect"));
			GetComponent<SphereCollider>().enabled = false;
			StartCoroutine(StartCheck());
			StartCoroutine(End());
		}
	
		// Update is called once per frame
		void Update () {
	
		}

		void OnTriggerEnter(Collider other)
		{
			if (other.CompareTag("PlayerHead") || other.CompareTag("PlayerBody"))
			{
				GetComponent<Collider>().enabled = false;
				Global.DealDamageTo(null, other.gameObject, new Damage {damage = 5});
			}
		}

		IEnumerator End()
		{
			yield return new WaitForSeconds(1.5f);
			Destroy(gameObject);
		}

		IEnumerator StartCheck()
		{
			yield return new WaitForSeconds(1);
			GetComponent<SphereCollider>().enabled = true;
		}
	}
}
